<?php

namespace QCRM\Http\Controllers\link;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class LinkController extends Controller
{
    public function index($status)
    {

    	return view('link/link', compact('status'));
    }

    public function link_voter(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);
        
        $Voters_list = $Voter->get();

        if($r->status == 'new'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        elseif($r->status == 'voter'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        elseif($r->status == 'nonvoter'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        endif;

        return view('link/success');  
    }

    public function create_beneficiary(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);
        
        $Voters_list = $Voter->get();

        if($r->status == 'new'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',2)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        elseif($r->status == 'voter'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',1)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        elseif($r->status == 'nonvoter'):
        foreach($Voters_list as $a)
        {
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')->where('voters_status',0)->where('status',1)
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);
        }
        endif;

        return view('link/success');  
    }

}