<?php

namespace QCRM\Http\Controllers\beneficiary;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class LivelihoodController extends Controller
{
    public function index()
    {
    	$Ched = new \QCRM\Models\beneficiary\Gip;
        $Barangay = new \QCRM\Models\beneficiary\Barangay;
        $Street = new \QCRM\Models\beneficiary\Street;

        $Barangays = $Barangay->get();
        $Streets = $Street->get();

        $Ched_list = $Ched->orderBy('created_at','desc')->where('status',1)->with(['fetch_availments'])->paginate(50);

        $service = 'livelihood';

        return view('beneficiary/Livelihood', compact('Ched_list','service','Barangays','Streets'));
    }

    public function beneficiary($id)
    {
    	$BeneficiaryModel = new \QCRM\Models\beneficiary\Gip;
    	$Beneficiary_OrigModel = new \QCRM\Models\beneficiary\Gip_availments;
    	//dd($id);

    	$Beneficiary = $BeneficiaryModel->where('id',$id)->first();
    	//dd($Beneficiary);
    	//dd($Beneficiary->mname);

    	return view('beneficiary/view', compact('Beneficiary','b','Beneficiary_Orig'));
    }
}