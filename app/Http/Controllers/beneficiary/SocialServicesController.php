<?php

namespace QCRM\Http\Controllers\beneficiary;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SocialServicesController extends Controller
{
    public function index()
    {
    	$BeneficiaryModel = new \QCRM\Models\beneficiary\Master;

    	$Beneficiary = $BeneficiaryModel->paginate(20);

    	return view('beneficiary/Services', compact('Beneficiary'));
    }

    public function beneficiary($service,$id)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Barangay = new \QCRM\Models\beneficiary\Barangay;
    	//dd($id);

        if($service == 'ched')
        {
           $Beneficiary = $Ched->where('id',$id)->first();
    	   $Availments = $Ched_availments->where('ched_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'qfs')
        {
           $Beneficiary = $Qfs->where('id',$id)->first();
           $Availments = $Qfs_availments->where('qfs_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'medical')
        {
           $Beneficiary = $Medical->where('id',$id)->first();
           $Availments = $Medical_availments->where('medical_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'financial')
        {
           $Beneficiary = $Financial->where('id',$id)->first();
           $Availments = $Financial_availments->where('financial_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'burial')
        {
           $Beneficiary = $Burial->where('id',$id)->first();
           $Availments = $Burial_availments->where('burial_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'dfa')
        {
           $Beneficiary = $Dfa->where('id',$id)->first();
           $Availments = $Dfa_availments->where('dfa_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'pcso')
        {
           $Beneficiary = $Pcso->where('id',$id)->first();
           $Availments = $Pcso_availments->where('pcso_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'legal')
        {
           $Beneficiary = $Legal->where('id',$id)->first();
           $Availments = $Legal_availments->where('legal_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'livelihood' or $service == 'gip')
        {
           $Beneficiary = $Gip->where('id',$id)->first();
           $Availments = $Gip_availments->where('gip_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
        elseif($service == 'college' or $service == 'ched')
        {
           $Beneficiary = $Ched->where('id',$id)->first();
           $Availments = $Ched_availments->where('ched_id',$id)->with(['fetch_officer'])->where('status',1)->get();
        }
    	
        $Barangays = $Barangay->get();
        //dd($Beneficiary);

    	


    	//dd($Beneficiary->mname);

    	return view('beneficiary/view', compact('Beneficiary','Availments','service','Barangays'));
    }

    public function search(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;

        //dd($r->all());

        if($r->service == 'ched')
        {
        	if($r->searchoption == 'name')
        	{
                $Beneficiary =
                $Ched->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
        	}
        	elseif($r->searchoption == 'address')
        	{
        		$Beneficiary = $Ched->where('address','like','%'.$r->search.'%')->where('status',1)->get();
        	}
        }
        elseif($r->service == 'qfs')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Qfs->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Qfs->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'medical')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Medical->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Medical->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'financial')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Financial->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Financial->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'burial')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Burial->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Burial->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'dfa')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Dfa->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Dfa->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'pcso')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Pcso->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Pcso->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'legal')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Legal->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Legal_availments->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'livelihood')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Gip->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Gip_availments->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }
        elseif($r->service == 'college')
        {
            if($r->searchoption == 'name')
            {
                $Beneficiary =
                $Ched->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Ched->where('address','like','%'.$r->search.'%')->where('status',1)->get();
            }
        }

        //dd($Beneficiary);

        $service = $r->service;


    	return view('beneficiary/search', compact('Beneficiary','service'));
    }

    public function search_voter(Request $r)
    {
    	$VotersModel = new \QCRM\Models\Voters;

    	//dd($r->searchoption);
    	$beneficiary_id = $r->beneficiary_id;
    	$beneficiary_fname = $r->fname;
    	$beneficiary_mname = $r->mname;
    	$beneficiary_lname = $r->lname;
    	$beneficiary_address = $r->address;
        $service = $r->service;
    	//dd($Requests);

    	if($r->searchoption == 'name')
    	{
    		$Voters = $VotersModel->where('name','like','%'.$r->search.'%')->where('status',1)->get();
    	}
    	elseif($r->searchoption == 'comelecid')
    	{
    		$Voters = $VotersModel->where('comelec_id_no', $r->search)->get();
    	}

        
    	//dd($Voters);

    	return view('voter/search', compact('Voters','beneficiary_id','beneficiary_fname','beneficiary_mname','beneficiary_lname','beneficiary_address','service'));
    }

    public function link_voter(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;


        //dd($r->all());

        if($r->service == 'ched')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'ched';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'qfs')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Qfs->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Qfs_availments->where('qfs_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'qfs';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'medical')
        {
        $Medical
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Medical->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Medical_availments->where('medical_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'medical';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'financial')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Financial_availments->where('financial_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'financial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'burial')
        {
        $Burial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Burial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Burial_availments->where('burial_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'burial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'dfa')
        {
        $Dfa
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Dfa->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Dfa_availments->where('dfa_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'dfa';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'pcso')
        {
        $Pcso
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Pcso->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Pcso_availments->where('pcso_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'pcso';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'legal')
        {
        $Legal
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Legal->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Legal_availments->where('legal_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'financial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'livelihood')
        {
        $Gip
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Gip->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Gip_availments->where('gip_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'livelihood';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'college')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id, 'voters_status' => 1]);

        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'ched';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }

        //dd($Beneficiary);

        //$service = $r->service;
/*
    	$BeneficiaryModel
    	->where('id',$r->beneficiary_id)
    	->update(['voters_id' => $r->voters_id]);*/



/*    	$Beneficiary = $BeneficiaryModel->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
    	//dd($Beneficiary);

    	$Beneficiary_Orig = $Beneficiary_OrigModel->where('lname',$Beneficiary->lname)->where('mname',$Beneficiary->mname)->where('fname',$Beneficiary->fname)->where('status',1)->get();


    	return view('beneficiary/view', compact('Beneficiary','Beneficiary_Orig'));*/
    }

    public function non_voter(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;


        //dd($r->all());

        if($r->service == 'ched')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'ched';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'qfs')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Qfs->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Qfs_availments->where('qfs_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'qfs';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'medical')
        {
        $Medical
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Medical->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Medical_availments->where('medical_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'medical';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'financial')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Financial_availments->where('financial_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'financial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'burial')
        {
        $Burial
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Burial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Burial_availments->where('burial_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'burial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'dfa')
        {
        $Dfa
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Dfa->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Dfa_availments->where('dfa_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'dfa';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'pcso')
        {
        $Pcso
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Pcso->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Pcso_availments->where('pcso_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'pcso';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'legal')
        {
        $Legal
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Legal->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Legal_availments->where('legal_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'financial';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'livelihood')
        {
        $Gip
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Gip->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Gip_availments->where('gip_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'livelihood';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }
        elseif($r->service == 'college')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => 0, 'voters_status' => 0]);

        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        $service = 'ched';

        return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
        }

        //dd($Beneficiary);

        //$service = $r->service;
/*
        $BeneficiaryModel
        ->where('id',$r->beneficiary_id)
        ->update(['voters_id' => $r->voters_id]);*/



/*      $Beneficiary = $BeneficiaryModel->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
        //dd($Beneficiary);

        $Beneficiary_Orig = $Beneficiary_OrigModel->where('lname',$Beneficiary->lname)->where('mname',$Beneficiary->mname)->where('fname',$Beneficiary->fname)->where('status',1)->get();


        return view('beneficiary/view', compact('Beneficiary','Beneficiary_Orig'));*/
    }

        public function unlink_voter(Request $r)
        {
            $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
            $Ched = new \QCRM\Models\beneficiary\Ched;
            $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
            $Dfa = new \QCRM\Models\beneficiary\Dfa;
            $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
            $Financial = new \QCRM\Models\beneficiary\Financial;
            $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
            $Medical = new \QCRM\Models\beneficiary\Medical;
            $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
            $Pcso = new \QCRM\Models\beneficiary\Pcso;
            $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
            $Burial = new \QCRM\Models\beneficiary\Burial;
            $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
            $Qfs = new \QCRM\Models\beneficiary\Qfs;
            $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
            $Gip = new \QCRM\Models\beneficiary\Gip;
            $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
            $Legal = new \QCRM\Models\beneficiary\Legal;
            $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;


            //dd($r->all());

            if($r->service == 'ched')
            {
            $Ched
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'ched';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'qfs')
            {
            $Financial
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Qfs->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Qfs_availments->where('qfs_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'qfs';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'medical')
            {
            $Medical
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Medical->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Medical_availments->where('medical_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'medical';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'financial')
            {
            $Financial
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Financial_availments->where('financial_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'financial';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'burial')
            {
            $Burial
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Burial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Burial_availments->where('burial_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'burial';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'dfa')
            {
            $Dfa
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Dfa->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Dfa_availments->where('dfa_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'dfa';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'pcso')
            {
            $Pcso
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Pcso->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Pcso_availments->where('pcso_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'pcso';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'legal')
            {
            $Legal
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Legal->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Legal_availments->where('legal_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'financial';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'livelihood')
            {
            $Gip
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Gip->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Gip_availments->where('gip_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'livelihood';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }
            elseif($r->service == 'college')
            {
            $Ched
            ->where('id',$r->beneficiary_id)
            ->update(['voters_id' => 0]);

            $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
            //dd($Beneficiary);

            $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

            $service = 'ched';

            return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));
            }

        }

    public function update_info(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        if($r->service == 'ched')
        {
            $Ched
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'ched';
        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'qfs')
        {
            $Qfs
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'qfs';
        $Beneficiary = $Qfs->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Qfs_availments->where('qfs_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'medical')
        {
            $Medical
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'medical';
        $Beneficiary = $Medical->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Medical_availments->where('medical_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'financial')
        {
            $Financial
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'financial';
        $Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Financial_availments->where('financial_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'burial')
        {
            $Burial
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'burial';
        $Beneficiary = $Burial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Burial_availments->where('burial_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'dfa')
        {
            $Dfa
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'dfa';
        $Beneficiary = $Dfa->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Dfa_availments->where('dfa_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'pcso')
        {
            $Pcso
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'pcso';
        $Beneficiary = $Pcso->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Pcso_availments->where('pcso_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'legal')
        {
            $Legal
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'legal';
        $Beneficiary = $Legal->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Legal_availments->where('legal_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'livelihood')
        {
            $Gip
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'livelihood';
        $Beneficiary = $Gip->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Gip_availments->where('gip_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'college')
        {
            $Ched
            ->where('id',$r->beneficiary_id)
            ->update([
                'fname' => $r->fname,
                'mname' => $r->mname,
                'lname' => $r->lname,
                'dob' => $r->dob,
                'address' => $r->address,
                'barangay_id' => $r->barangay_id,
                'district' => $r->district,
                'mobile_number' => $r->mobile_number,
                'email' => $r->email
            ]);

            if(!empty($r->voter_id))
            {
                $Voters
                ->where('id',$r->voter_id)
                ->update([
                    'id_no' => $r->idno,
                    'comelec_id_no' => $r->comelecid,
                    'precint' => $r->precint
                ]);
            }

        $service = 'ched';
        $Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        $Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }

    	/*$BeneficiaryModel
    	->where('id',$r->beneficiary_id)
    	->update([
    		'fname' => $r->fname,
    		'mname' => $r->mname,
    		'lname' => $r->lname,
    		'dob' => $r->dob,
    		'address' => $r->address,
    		'district' => $r->district,
    		'mobile_number' => $r->mobile_number,
    		'email' => $r->email
    	]);

    	if(!empty($r->voter_id))
    	{
    		$VotersModel
    		->where('id',$r->voter_id)
    		->update([
    			'id_no' => $r->idno,
    			'comelec_id_no' => $r->comelecid,
    			'precint' => $r->precint
    		]);
    	}*/




        //return Redirect()->route('beneficiary_view', compact('Beneficiary','Availments','service'));


    }

    public function delete_beneficiary(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;


        if($r->service == 'ched')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        //dd($r->all());
        $Beneficiary = $Ched->where('status',1)->paginate(20);

        return Redirect()->route('ched', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'qfs')
        {
        $Qfs
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Qfs->where('status',1)->paginate(20);

        return Redirect()->route('qfs', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'medical')
        {
        $Medical
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Medical->where('status',1)->paginate(20);

        return Redirect()->route('medical', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'financial')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Financial->where('status',1)->paginate(20);

        return Redirect()->route('financial', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'burial')
        {
        $Burial
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Burial->where('status',1)->paginate(20);

        return Redirect()->route('burial', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'dfa')
        {
        $Dfa
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Dfa->where('status',1)->paginate(20);

        return Redirect()->route('dfa', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'pcso')
        {
        $Pcso
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Pcso->where('status',1)->paginate(20);

        return Redirect()->route('pcso', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'legal')
        {
        $Financial
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Financial->where('status',1)->paginate(20);

        return Redirect()->route('legal', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'livelihood')
        {
        $Gip
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Gip->where('status',1)->paginate(20);

        return Redirect()->route('livelihood', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }
        elseif($r->service == 'college')
        {
        $Ched
        ->where('id',$r->beneficiary_id)
        ->update([
            'status' => 0
        ]);

        $Beneficiary = $Ched->where('status',1)->paginate(20);

        return Redirect()->route('ched', compact('Beneficiary'))->with('msg', 'Beneficiary Deleted Succesfully!');
        }

/*    	$BeneficiaryModel
    	->where('id',$r->beneficiary_id)
    	->update([
    		'status' => 0
    	]);

    	$Beneficiary = $BeneficiaryModel->paginate(20);*/
/*    	$Beneficiary = $BeneficiaryModel->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();
    	//dd($Beneficiary);

    	$Beneficiary_Orig = $Beneficiary_OrigModel->where('lname',$Beneficiary->lname)->where('mname',$Beneficiary->mname)->where('fname',$Beneficiary->fname)->where('status',1)->get();*/

    }

    public function update_availment(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        if($r->service == 'ched')
        {
            //dd($r->beneficiary_id);
        $Ched_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'qfs')
        {
        $Qfs_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'medical')
        {
        $Medical_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'financial')
        {
        $Financial_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'burial')
        {
        $Burial_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'dfa')
        {
        $Dfa_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'pcso')
        {
        $Pcso_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'legal')
        {
        $Legal_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'livelihood')
        {
        $Gip_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }
        elseif($r->service == 'college')
        {
        $Ched_availments
        ->where('id',$r->beneficiary_id)
        ->update([
            'request_date' => $r->request_date,
            'service_type' => $r->service_type,
            'amount' => $r->amount,
            'remarks' => $r->remarks
        ]);

        }

/*    	$Beneficiary_OrigModel
    	->where('id',$r->availment_id)
    	->update([
    		'request_date' => $r->request_date,
    		'service_type' => $r->service_type,
    		'amount' => $r->amount,
    		'remarks' => $r->remarks
    	]);
*/
    	return Redirect::back()->with('avail_msg', 'Success');
    }

    public function delete_availment(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        if($r->service == 'ched')
        {
        $Ched_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'qfs')
        {
        $Qfs_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'medical')
        {
        $Medical_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'financial')
        {
        $Financial_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'burial')
        {
        $Burial_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'dfa')
        {
        $Dfa_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'pcso')
        {
        $Pcso_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'legal')
        {
        $Legal_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'livelihood')
        {
        $Gip_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }
        elseif($r->service == 'college')
        {
        $Ched_availments
        ->where('id',$r->availment_id)
        ->update([
            'status' => 0
        ]);

        }

/*    	$Beneficiary_OrigModel
    	->where('id',$r->availment_id)
    	->update([
    		'status' => 0
    	]);
*/
    	return Redirect::back()->with('del_msg', 'Success');
    }

    public function add_availment(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;


	   	//dd($r->dob);

        if($r->service == 'ched')
        {
        $Ched_availments->name = !empty($r->fname)?$r->lname: ''.', '.!empty($r->fname)?$r->lname: ''.' '.!empty($r->mname)?$r->mname: '';
        $Ched_availments->birthday = date('Y-m-d', strtotime($r->dob));
        $Ched_availments->address = !empty($r->address)?$r->address: ' ';
        $Ched_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
/*        $Ched_availments->district = !empty($r->district)?$r->district: ' ';*/
        $Ched_availments->gender = !empty($r->sex)?$r->sex: ' ';
        $Ched_availments->contact_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched_availments->email = !empty($r->email)?$r->email: ' ';
        $Ched_availments->date = date('Y-m-d', strtotime($r->request_date));
        $Ched_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
/*        $Ched_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';*/
        $Ched_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched_availments->school_yr = !empty($r->school_yr)?$r->school_yr: ' ';
        $Ched_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched_availments->ched_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Ched_availments->save();

        }
        elseif($r->service == 'qfs')
        {
        $Qfs_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Qfs_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Qfs_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Qfs_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Qfs_availments->address = !empty($r->address)?$r->address: ' ';
        $Qfs_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Qfs_availments->district = !empty($r->district)?$r->district: ' ';
        $Qfs_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Qfs_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Qfs_availments->email = !empty($r->email)?$r->email: ' ';
        $Qfs_availments->date = date('Y-m-d', strtotime($r->request_date));
        $Qfs_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Qfs_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Qfs_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Qfs_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Qfs_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Qfs_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Qfs_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Qfs_availments->financial_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Qfs_availments->save();

        }
        elseif($r->service == 'medical')
        {
        $Medical_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Medical_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Medical_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Medical_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Medical_availments->address = !empty($r->address)?$r->address: ' ';
        $Medical_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Medical_availments->district = !empty($r->district)?$r->district: ' ';
        $Medical_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Medical_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Medical_availments->email = !empty($r->email)?$r->email: ' ';
        $Medical_availments->request_date = date('Y-m-d', strtotime($r->request_date)); 
        $Medical_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';  
        $Medical_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Medical_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Medical_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Medical_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Medical_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Medical_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Medical_availments->medical_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Medical_availments->save();

        }
        elseif($r->service == 'financial')
        {
        $Financial_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Financial_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Financial_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Financial_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Financial_availments->address = !empty($r->address)?$r->address: ' ';
        $Financial_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Financial_availments->district = !empty($r->district)?$r->district: ' ';
        $Financial_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Financial_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Financial_availments->email = !empty($r->email)?$r->email: ' ';
        $Financial_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Financial_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Financial_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Financial_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Financial_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Financial_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Financial_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Financial_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Financial_availments->financial_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Financial_availments->save();

        }
        elseif($r->service == 'burial')
        {
        $Burial_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Burial_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Burial_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Burial_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Burial_availments->address = !empty($r->address)?$r->address: ' ';
        $Burial_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Burial_availments->district = !empty($r->district)?$r->district: ' ';
        $Burial_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Burial_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Burial_availments->email = !empty($r->email)?$r->email: ' ';
        $Burial_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Burial_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Burial_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Burial_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Burial_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Burial_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Burial_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Burial_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Burial_availments->burial_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Burial_availments->save();

        }
        elseif($r->service == 'dfa')
        {
        $Dfa_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Dfa_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Dfa_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Dfa_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Dfa_availments->address = !empty($r->address)?$r->address: ' ';
        $Dfa_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Dfa_availments->district = !empty($r->district)?$r->district: ' ';
        $Dfa_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Dfa_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Dfa_availments->email = !empty($r->email)?$r->email: ' ';
        $Dfa_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Dfa_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Dfa_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Dfa_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Dfa_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Dfa_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Dfa_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Dfa_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Dfa_availments->dfa_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Dfa_availments->save();

        }
        elseif($r->service == 'pcso')
        {
        $Pcso_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Pcso_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Pcso_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Pcso_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Pcso_availments->address = !empty($r->address)?$r->address: ' ';
        $Pcso_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Pcso_availments->district = !empty($r->district)?$r->district: ' ';
        $Pcso_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Pcso_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Pcso_availments->email = !empty($r->email)?$r->email: ' ';
        $Pcso_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Pcso_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Pcso_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Pcso_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Pcso_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Pcso_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Pcso_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Pcso_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Pcso_availments->pcso_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Pcso_availments->save();

        }
        elseif($r->service == 'legal')
        {
        $Legal_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Legal_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Legal_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Legal_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Legal_availments->address = !empty($r->address)?$r->address: ' ';
        $Legal_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Legal_availments->district = !empty($r->district)?$r->district: ' ';
        $Legal_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Legal_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Legal_availments->email = !empty($r->email)?$r->email: ' ';
        $Legal_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Legal_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Legal_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Legal_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Legal_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Legal_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Legal_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Legal_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Legal_availments->financial_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Legal_availments->save();

        }
        elseif($r->service == 'livelihood')
        {
        $Gip_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Gip_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Gip_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Gip_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Gip_availments->address = !empty($r->address)?$r->address: ' ';
        $Gip_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Gip_availments->district = !empty($r->district)?$r->district: ' ';
        $Gip_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Gip_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Gip_availments->email = !empty($r->email)?$r->email: ' ';
        $Gip_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Gip_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Gip_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Gip_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Gip_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Gip_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Gip_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Gip_availments->financial_id = !empty($r->remarks)?$r->remarks: ' ';
        $Gip_availments->ched_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Gip_availments->save();

        }
        elseif($r->service == 'college')
        {
        $Ched_availments->fname = !empty($r->fname)?$r->fname: ' ';
        $Ched_availments->mname = !empty($r->mname)?$r->mname: ' ';
        $Ched_availments->lname = !empty($r->lname)?$r->lname: ' ';
        $Ched_availments->dob = date('Y-m-d', strtotime($r->dob));
        $Ched_availments->address = !empty($r->address)?$r->address: ' ';
        $Ched_availments->barangay = !empty($r->barangay)?$r->barangay: ' ';
        $Ched_availments->district = !empty($r->district)?$r->district: ' ';
        $Ched_availments->sex = !empty($r->sex)?$r->sex: ' ';
        $Ched_availments->mobile_no = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched_availments->email = !empty($r->email)?$r->email: ' ';
        $Ched_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Ched_availments->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Ched_availments->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Ched_availments->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched_availments->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched_availments->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched_availments->ched_id = !empty($r->beneficiary_id)?$r->beneficiary_id: ' ';
        $Ched_availments->save();

        }


/*    	$Beneficiary_OrigModel->fname = !empty($r->fname)?$r->fname: ' ';
    	$Beneficiary_OrigModel->mname = !empty($r->mname)?$r->mname: ' ';
    	$Beneficiary_OrigModel->lname = !empty($r->lname)?$r->lname: ' ';
    	$Beneficiary_OrigModel->dob = date('Y-m-d', strtotime($r->dob));
    	$Beneficiary_OrigModel->address = !empty($r->address)?$r->address: ' ';
    	$Beneficiary_OrigModel->barangay = !empty($r->barangay)?$r->barangay: ' ';
    	$Beneficiary_OrigModel->district = !empty($r->district)?$r->district: ' ';
    	$Beneficiary_OrigModel->sex = !empty($r->sex)?$r->sex: ' ';
    	$Beneficiary_OrigModel->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
    	$Beneficiary_OrigModel->email = !empty($r->email)?$r->email: ' ';
    	$Beneficiary_OrigModel->request_date = date('Y-m-d', strtotime($r->request_date));
    	$Beneficiary_OrigModel->service_type = !empty($r->service_type)?$r->service_type: ' ';
    	$Beneficiary_OrigModel->particulars = !empty($r->particulars)?$r->particulars: ' ';
    	$Beneficiary_OrigModel->institution = !empty($r->institution)?$r->institution: ' ';
    	$Beneficiary_OrigModel->amount = !empty($r->amount)?$r->amount: ' ';
    	$Beneficiary_OrigModel->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
    	$Beneficiary_OrigModel->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
    	$Beneficiary_OrigModel->remarks = !empty($r->remarks)?$r->remarks: ' ';
    	$Beneficiary_OrigModel->save();*/





    	return Redirect::back()->with('add_msg', 'Success');
    }

    public function add_beneficiary(Request $r)
    {

       // dd($r->all());
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $address_details = $Barangay->where('id',$r->barangay_id)->first();
	   	//dd($r->all());

    if($r->service == 'ched')
        {
        $Ched->fname = !empty($r->fname)?$r->fname: ' ';
        $Ched->mname = !empty($r->mname)?$r->mname: ' ';
        $Ched->lname = !empty($r->lname)?$r->lname: ' ';
        $Ched->dob = date('Y-m-d', strtotime($r->dob));
        $Ched->address = !empty($r->address)?$r->address: ' ';
        $Ched->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Ched->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Ched->district = !empty($address_details->district)?$address_details->district: ' ';
        $Ched->sex = !empty($r->sex)?$r->sex: ' ';
        $Ched->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched->email = !empty($r->email)?$r->email: ' ';
        $Ched->request_date = date('Y-m-d', strtotime($r->request_date));
        $Ched->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Ched->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Ched->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched->voters_status = 2;
        $Ched->save();

        $Ched_availments->date = date('Y-m-d', strtotime($r->request_date));
        $Ched_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched_availments->ched_id = $Ched->id;
        $Ched_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched_availments->save();

        $service = 'ched';

        //dd($Ched->id);
        //$Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'qfs')
        {

           // dd($r->all());
        $Qfs->fname = !empty($r->fname)?$r->fname: ' ';
        $Qfs->mname = !empty($r->mname)?$r->mname: ' ';
        $Qfs->lname = !empty($r->lname)?$r->lname: ' ';
        $Qfs->dob = date('Y-m-d', strtotime($r->dob));
        $Qfs->address = !empty($r->address)?$r->address: ' ';
        $Qfs->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Qfs->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Qfs->district = !empty($address_details->district)?$address_details->district: ' ';
        $Qfs->sex = !empty($r->sex)?$r->sex: ' ';
        $Qfs->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Qfs->email = !empty($r->email)?$r->email: ' ';
        $Qfs->date = date('Y-m-d', strtotime($r->request_date));
        $Qfs->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Qfs->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Qfs->institution = !empty($r->institution)?$r->institution: ' ';
        $Qfs->amount = !empty($r->amount)?$r->amount: ' ';
        $Qfs->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Qfs->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Qfs->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Qfs->voters_status = 2;
        $Qfs->save();

        $Qfs_availments->date = date('Y-m-d', strtotime($r->request_date));
        $Qfs_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Qfs_availments->qfs_id = $Qfs->id;
        $Qfs_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Qfs_availments->save();

        $service = 'qfs';
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'medical')
        {
        $Medical->fname = !empty($r->fname)?$r->fname: ' ';
        $Medical->mname = !empty($r->mname)?$r->mname: ' ';
        $Medical->lname = !empty($r->lname)?$r->lname: ' ';
        $Medical->dob = date('Y-m-d', strtotime($r->dob));
        $Medical->address = !empty($r->address)?$r->address: ' ';
        $Medical->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Medical->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Medical->district = !empty($address_details->district)?$address_details->district: ' ';
        $Medical->sex = !empty($r->sex)?$r->sex: ' ';
        $Medical->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Medical->email = !empty($r->email)?$r->email: ' ';
        $Medical->request_date = date('Y-m-d', strtotime($r->request_date));
        $Medical->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Medical->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Medical->institution = !empty($r->institution)?$r->institution: ' ';
        $Medical->amount = !empty($r->amount)?$r->amount: ' ';
        $Medical->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Medical->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Medical->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Medical->voters_status = 2;
        $Medical->save();

        $Medical_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Medical_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Medical_availments->medical_id = $Medical->id;
        $Medical_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Medical_availments->save();

        $service = 'medical';
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'financial')
        {
        $Financial->fname = !empty($r->fname)?$r->fname: ' ';
        $Financial->mname = !empty($r->mname)?$r->mname: ' ';
        $Financial->lname = !empty($r->lname)?$r->lname: ' ';
        $Financial->dob = date('Y-m-d', strtotime($r->dob));
        $Financial->address = !empty($r->address)?$r->address: ' ';
        $Financial->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Financial->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Financial->district = !empty($address_details->district)?$address_details->district: ' ';
        $Financial->sex = !empty($r->sex)?$r->sex: ' ';
        $Financial->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Financial->email = !empty($r->email)?$r->email: ' ';
        $Financial->request_date = date('Y-m-d', strtotime($r->request_date));
        $Financial->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Financial->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Financial->institution = !empty($r->institution)?$r->institution: ' ';
        $Financial->amount = !empty($r->amount)?$r->amount: ' ';
        $Financial->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Financial->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Financial->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Financial->voters_status = 2;
        $Financial->save();

        $Financial_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Financial_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Financial_availments->financial_id = $Financial->id;
        $Financial_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Financial_availments->save();

        $service = 'financial';
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'burial')
        {
        $Burial->fname = !empty($r->fname)?$r->fname: ' ';
        $Burial->mname = !empty($r->mname)?$r->mname: ' ';
        $Burial->lname = !empty($r->lname)?$r->lname: ' ';
        $Burial->dob = date('Y-m-d', strtotime($r->dob));
        $Burial->address = !empty($r->address)?$r->address: ' ';
        $Burial->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Burial->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Burial->district = !empty($address_details->district)?$address_details->district: ' ';
        $Burial->sex = !empty($r->sex)?$r->sex: ' ';
        $Burial->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Burial->email = !empty($r->email)?$r->email: ' ';
        $Burial->request_date = date('Y-m-d', strtotime($r->request_date));
        $Burial->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Burial->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Burial->institution = !empty($r->institution)?$r->institution: ' ';
        $Burial->amount = !empty($r->amount)?$r->amount: ' ';
        $Burial->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Burial->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Burial->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Burial->voters_status = 2;
        $Burial->save();

        $Burial_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Burial_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Burial_availments->burial_id = $Burial->id;
        $Burial_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Burial_availments->save();

        $service = 'burial';
        //$Beneficiary = $Burial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Burial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'dfa')
        {
        $Dfa->fname = !empty($r->fname)?$r->fname: ' ';
        $Dfa->mname = !empty($r->mname)?$r->mname: ' ';
        $Dfa->lname = !empty($r->lname)?$r->lname: ' ';
        $Dfa->dob = date('Y-m-d', strtotime($r->dob));
        $Dfa->address = !empty($r->address)?$r->address: ' ';
        $Dfa->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Dfa->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Dfa->district = !empty($address_details->district)?$address_details->district: ' ';
        $Dfa->sex = !empty($r->sex)?$r->sex: ' ';
        $Dfa->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Dfa->email = !empty($r->email)?$r->email: ' ';
        $Dfa->request_date = date('Y-m-d', strtotime($r->request_date));
        $Dfa->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Dfa->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Dfa->institution = !empty($r->institution)?$r->institution: ' ';
        $Dfa->amount = !empty($r->amount)?$r->amount: ' ';
        $Dfa->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Dfa->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Dfa->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Dfa->voters_status = 2;
        $Dfa->save();

        $Dfa_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Dfa_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Dfa_availments->dfa_id = $Dfa->id;
        $Dfa_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Dfa_availments->save();

        $service = 'dfa';
        //$Beneficiary = $Dfa->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Dfa_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'pcso')
        {
        $Pcso->fname = !empty($r->fname)?$r->fname: ' ';
        $Pcso->mname = !empty($r->mname)?$r->mname: ' ';
        $Pcso->lname = !empty($r->lname)?$r->lname: ' ';
        $Pcso->dob = date('Y-m-d', strtotime($r->dob));
        $Pcso->address = !empty($r->address)?$r->address: ' ';
        $Pcso->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Pcso->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Pcso->district = !empty($address_details->district)?$address_details->district: ' ';
        $Pcso->sex = !empty($r->sex)?$r->sex: ' ';
        $Pcso->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Pcso->email = !empty($r->email)?$r->email: ' ';
        $Pcso->request_date = date('Y-m-d', strtotime($r->request_date));
        $Pcso->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Pcso->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Pcso->institution = !empty($r->institution)?$r->institution: ' ';
        $Pcso->amount = !empty($r->amount)?$r->amount: ' ';
        $Pcso->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Pcso->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Pcso->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Pcso->voters_status = 2;
        $Pcso->save();

        $Pcso_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Pcso_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Pcso_availments->pcso_id = $Pcso->id;
        $Pcso_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Pcso_availments->save();

        $service = 'pcso';
        //$Beneficiary = $Pcso->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Pcso_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'legal')
        {
        $Legal->fname = !empty($r->fname)?$r->fname: ' ';
        $Legal->mname = !empty($r->mname)?$r->mname: ' ';
        $Legal->lname = !empty($r->lname)?$r->lname: ' ';
        $Legal->dob = date('Y-m-d', strtotime($r->dob));
        $Legal->address = !empty($r->address)?$r->address: ' ';
        $Legal->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Legal->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Legal->district = !empty($address_details->district)?$address_details->district: ' ';
        $Legal->sex = !empty($r->sex)?$r->sex: ' ';
        $Legal->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Legal->email = !empty($r->email)?$r->email: ' ';
        $Legal->request_date = date('Y-m-d', strtotime($r->request_date));
        $Legal->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Legal->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Legal->institution = !empty($r->institution)?$r->institution: ' ';
        $Legal->amount = !empty($r->amount)?$r->amount: ' ';
        $Legal->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Legal->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Legal->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Legal->voters_status = 2;
        $Legal->save();

        $Legal_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Legal_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Legal_availments->legal_id = $Legal->id;
        $Legal_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Legal_availments->save();

        $service = 'legal';
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'livelihood')
        {
        $Gip->fname = !empty($r->fname)?$r->fname: ' ';
        $Gip->mname = !empty($r->mname)?$r->mname: ' ';
        $Gip->lname = !empty($r->lname)?$r->lname: ' ';
        $Gip->dob = date('Y-m-d', strtotime($r->dob));
        $Gip->address = !empty($r->address)?$r->address: ' ';
        $Gip->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Gip->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Gip->district = !empty($address_details->district)?$address_details->district: ' ';
        $Gip->sex = !empty($r->sex)?$r->sex: ' ';
        $Gip->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Gip->email = !empty($r->email)?$r->email: ' ';
        $Gip->request_date = date('Y-m-d', strtotime($r->request_date));
        $Gip->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Gip->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Gip->institution = !empty($r->institution)?$r->institution: ' ';
        $Gip->amount = !empty($r->amount)?$r->amount: ' ';
        $Gip->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Gip->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Gip->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Gip->voters_status = 2;
        $Gip->save();

        $Gip_availments->request_date = date('Y-m-d', strtotime($r->request_date));
        $Gip_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Gip_availments->gip_id = $Gip->id;
        $Gip_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Gip_availments->save();

        $service = 'livelihood';
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }
        elseif($r->service == 'college')
        {
        $Ched->fname = !empty($r->fname)?$r->fname: ' ';
        $Ched->mname = !empty($r->mname)?$r->mname: ' ';
        $Ched->lname = !empty($r->lname)?$r->lname: ' ';
        $Ched->dob = date('Y-m-d', strtotime($r->dob));
        $Ched->address = !empty($r->address)?$r->address: ' ';
        $Ched->barangay = !empty($address_details->barangay)?$address_details->barangay: ' ';
        $Ched->barangay_id = !empty($address_details->id)?$address_details->id: ' ';  
        $Ched->district = !empty($address_details->district)?$address_details->district: ' ';
        $Ched->sex = !empty($r->sex)?$r->sex: ' ';
        $Ched->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched->email = !empty($r->email)?$r->email: ' ';
        $Ched->request_date = date('Y-m-d', strtotime($r->request_date));
        $Ched->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Ched->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Ched->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched->voters_status = 2;
        $Ched->save();

        $Ched_availments->date = date('Y-m-d', strtotime($r->request_date));
        $Ched_availments->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched_availments->ched_id = $Ched->id;
        $Ched_availments->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched_availments->save();

        $service = 'ched';
        //$Beneficiary = $Ched->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Ched_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect::back()->with('msg', 'Success');
        }

/*    	$Beneficiary_Model->fname = !empty($r->fname)?$r->fname: ' ';
    	$Beneficiary_Model->mname = !empty($r->mname)?$r->mname: ' ';
    	$Beneficiary_Model->lname = !empty($r->lname)?$r->lname: ' ';
    	$Beneficiary_Model->dob = date('Y-m-d', strtotime($r->dob));
    	$Beneficiary_Model->address = !empty($r->address)?$r->address: ' ';
    	$Beneficiary_Model->barangay = !empty($r->barangay)?$r->barangay: ' ';
    	$Beneficiary_Model->district = !empty($r->district)?$r->district: ' ';
    	$Beneficiary_Model->sex = !empty($r->sex)?$r->sex: ' ';
    	$Beneficiary_Model->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
    	$Beneficiary_Model->email = !empty($r->email)?$r->email: ' ';
    	$Beneficiary_Model->request_date = date('Y-m-d', strtotime($r->request_date));
    	$Beneficiary_Model->service_type = !empty($r->service_type)?$r->service_type: ' ';
    	$Beneficiary_Model->particulars = !empty($r->particulars)?$r->particulars: ' ';
    	$Beneficiary_Model->institution = !empty($r->institution)?$r->institution: ' ';
    	$Beneficiary_Model->amount = !empty($r->amount)?$r->amount: ' ';
    	$Beneficiary_Model->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
    	$Beneficiary_Model->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
    	$Beneficiary_Model->remarks = !empty($r->remarks)?$r->remarks: ' ';
    	$Beneficiary_Model->save();*/


    	//$Beneficiary = $Beneficiary_Model->where('lname',$Beneficiary_Model->lname)->where('mname',$Beneficiary_Model->mname)->where('fname',$Beneficiary_Model->fname)->where('address',$Beneficiary_Model->address)->where('status',1)->with(['voters_info'])->first(); //dd($Beneficiary);

    	//$Beneficiary_Orig = $Beneficiary_OrigModel->where('lname',$Beneficiary->lname)->where('mname',$Beneficiary->mname)->where('fname',$Beneficiary->fname)->where('status',1)->get();


    	//dd($Beneficiary->mname);

    	//return view('beneficiary/view', compact('Beneficiary','b','Beneficiary_Orig'));
    //}
    }

    public function master()
    {

        return view('beneficiary/Master_search');
    }

    public function search_all(Request $r)
    {
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;

/*                $Ched_search =
                $Ched
                ->get();

                $Qfs_search =
                $Qfs
                ->get();

                $Medical_search =
                $Medical
                ->get();

                $Financial_search =
                $Financial
                ->get();

                $Burial_search =
                $Burial
                ->get();

                $Dfa_search =
                $Dfa
                ->get();

                $Pcso_search =
                $Pcso
                ->get();

                $Legal_search =
                $Legal
                ->get();

                $Gip_search =
                $Gip
                ->get();

                $Beneficiary = new \Illuminate\Database\Eloquent\Collection;
                $Beneficiary = $Beneficiary->merge($Ched_search);
                $Beneficiary = $Beneficiary->merge($Qfs_search);
                $Beneficiary = $Beneficiary->merge($Medical_search);
                $Beneficiary = $Beneficiary->merge($Financial_search);
                $Beneficiary = $Beneficiary->merge($Burial_search);
                $Beneficiary = $Beneficiary->merge($Dfa_search);
                $Beneficiary = $Beneficiary->merge($Pcso_search);
                $Beneficiary = $Beneficiary->merge($Legal_search);
                $Beneficiary = $Beneficiary->merge($Gip_search);
                $Beneficiary = $Beneficiary->random(30);*/

        //$r->search = 'ab';
            if($r->searchoption == 'name')
            {
                $Ched_search =
                $Ched->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Qfs_search =
                $Qfs->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Medical_search =
                $Medical->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Financial_search =
                $Financial->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Burial_search =
                $Burial->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Dfa_search =
                $Dfa->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Pcso_search =
                $Pcso->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Legal_search =
                $Legal->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Gip_search =
                $Gip->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->where('status',1)
                ->get();

                $Beneficiary = new \Illuminate\Database\Eloquent\Collection;
                $Beneficiary = $Beneficiary->merge($Ched_search);
                $Beneficiary = $Beneficiary->merge($Qfs_search);
                $Beneficiary = $Beneficiary->merge($Medical_search);
                $Beneficiary = $Beneficiary->merge($Financial_search);
                $Beneficiary = $Beneficiary->merge($Burial_search);
                $Beneficiary = $Beneficiary->merge($Dfa_search);
                $Beneficiary = $Beneficiary->merge($Pcso_search);
                $Beneficiary = $Beneficiary->merge($Legal_search);
                $Beneficiary = $Beneficiary->merge($Gip_search);

                //dd($Beneficiary);
            }
            elseif($r->searchoption == 'address')
            {
                $Ched_search = $Ched->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Qfs_search = $Qfs->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Medical_search = $Medical->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Financial_search = $Financial->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Burial_search = $Burial->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Dfa_search = $Dfa->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Pcso_search = $Pcso->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Legal_search = $Legal->where('address','like','%'.$r->search.'%')->where('status',1)->get();
                $Gip_search = $Gip->where('address','like','%'.$r->search.'%')->where('status',1)->get();

                $Beneficiary = new \Illuminate\Database\Eloquent\Collection;
                $Beneficiary = $Beneficiary->merge($Ched_search);
                $Beneficiary = $Beneficiary->merge($Qfs_search);
                $Beneficiary = $Beneficiary->merge($Medical_search);
                $Beneficiary = $Beneficiary->merge($Financial_search);
                $Beneficiary = $Beneficiary->merge($Burial_search);
                $Beneficiary = $Beneficiary->merge($Dfa_search);
                $Beneficiary = $Beneficiary->merge($Pcso_search);
                $Beneficiary = $Beneficiary->merge($Legal_search);
                $Beneficiary = $Beneficiary->merge($Gip_search);
            }

        
        return view('beneficiary/Master_search_view', compact('Beneficiary'));



    }    

public function search_reverse_all(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;

        //$r->search = 'ab';
            if($r->searchoption == 'name')
            {
                $Ched_search =
                $Ched_availments->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->whereNULL('ched_id')
                ->where('status',1)
                ->get();

                $Qfs_search =
                $Qfs_availments->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->whereNULL('qfs_id')
                ->where('status',1)
                ->get();

                $Medical_search =
                $Medical_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('medical_id')
                ->where('status',1)
                ->get();

                $Financial_search =
                $Financial_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('financial_id')
                ->where('status',1)
                ->get();

                $Burial_search =
                $Burial_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('burial_id')
                ->where('status',1)
                ->get();

                $Dfa_search =
                $Dfa_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('dfa_id')
                ->where('status',1)
                ->get();

                $Pcso_search =
                $Pcso_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('pcso_id')
                ->where('status',1)
                ->get();

                $Legal_search =
                $Legal_availments->where(function($query) use ($r){
                $query
                ->where('fname','like','%'.$r->search.'%')
                ->orwhere('mname','like','%'.$r->search.'%')
                ->orwhere('lname','like','%'.$r->search.'%');
                })
                ->whereNULL('legal_id')
                ->where('status',1)
                ->get();

                $Gip_search =
                $Gip_availments->where(function($query) use ($r){
                $query
                ->where('name','like','%'.$r->search.'%');
                })
                ->whereNULL('gip_id')
                ->where('status',1)
                ->get();

                $Beneficiary = new \Illuminate\Database\Eloquent\Collection;
                $Beneficiary = $Beneficiary->merge($Ched_search);
                $Beneficiary = $Beneficiary->merge($Qfs_search);
                $Beneficiary = $Beneficiary->merge($Medical_search);
                $Beneficiary = $Beneficiary->merge($Financial_search);
                $Beneficiary = $Beneficiary->merge($Burial_search);
                $Beneficiary = $Beneficiary->merge($Dfa_search);
                $Beneficiary = $Beneficiary->merge($Pcso_search);
                $Beneficiary = $Beneficiary->merge($Legal_search);
                $Beneficiary = $Beneficiary->merge($Gip_search);

                //dd($Beneficiary);
            }
            elseif($r->searchoption == 'address')
            {
                $Ched_search = $Ched_availments->where('address','like','%'.$r->search.'%')->whereNULL('ched_id')->where('status',1)->get();
                $Qfs_search = $Qfs_availments->where('address','like','%'.$r->search.'%')->whereNULL('qfs_id')->where('status',1)->get();
                $Medical_search = $Medical_availments->where('address','like','%'.$r->search.'%')->whereNULL('medical_id')->where('status',1)->get();
                $Financial_search = $Financial_availments->where('address','like','%'.$r->search.'%')->whereNULL('financial_id')->where('status',1)->get();
                $Burial_search = $Burial_availments->where('address','like','%'.$r->search.'%')->whereNULL('burial_id')->where('status',1)->get();
                $Dfa_search = $Dfa_availments->where('address','like','%'.$r->search.'%')->whereNULL('dfa_id')->where('status',1)->get();
                $Pcso_search = $Pcso_availments->where('address','like','%'.$r->search.'%')->whereNULL('pcso_id')->where('status',1)->get();
                $Legal_search = $Legal_availments->where('address','like','%'.$r->search.'%')->whereNULL('legal_id')->where('status',1)->get();
                $Gip_search = $Gip_availments->where('address','like','%'.$r->search.'%')->whereNULL('gip_id')->where('status',1)->get();

                $Beneficiary = new \Illuminate\Database\Eloquent\Collection;
                $Beneficiary = $Beneficiary->merge($Ched_search);
                $Beneficiary = $Beneficiary->merge($Qfs_search);
                $Beneficiary = $Beneficiary->merge($Medical_search);
                $Beneficiary = $Beneficiary->merge($Financial_search);
                $Beneficiary = $Beneficiary->merge($Burial_search);
                $Beneficiary = $Beneficiary->merge($Dfa_search);
                $Beneficiary = $Beneficiary->merge($Pcso_search);
                $Beneficiary = $Beneficiary->merge($Legal_search);
                $Beneficiary = $Beneficiary->merge($Gip_search);
            }

        
        return view('reverse/search_all_reverse_view', compact('Beneficiary'));



    }

}