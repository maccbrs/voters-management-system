<?php

namespace QCRM\Http\Controllers\beneficiary;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ReverseController extends Controller
{
    public function index($service)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;
        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Barangays = $Barangay->get();

        //dd($service);
        if($service == 'ched')
        {
/*            echo 'test';*/
    	   $Beneficiary = $Ched_availments->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'qfs')
        {
            $Beneficiary = $Qfs_availments->orderBy('id')->whereNULL('qfs_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'medical')
        {
            $Beneficiary = $Medical_availments->orderBy('id')->whereNULL('medical_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'financial')
        {
            $Beneficiary = $Financial_availments->orderBy('id')->whereNULL('financial_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'burial')
        {
            $Beneficiary = $Burial_availments->orderBy('id')->whereNULL('burial_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'dfa')
        {
            $Beneficiary = $Dfa_availments->orderBy('id')->whereNULL('dfa_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'pcso')
        {
            $Beneficiary = $Pcso_availments->orderBy('id')->whereNULL('pcso_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'legal')
        {
            $Beneficiary = $Legal_availments->orderBy('id')->whereNULL('legal_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'livelihood')
        {
            $Beneficiary = $Gip_availments->orderBy('id')->whereNULL('gip_id')->where('status',1)->paginate(100);
        }
        elseif($service == 'college')
        {
            $Beneficiary = $Ched_availments->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
        }


    	return view('reverse/reverse', compact('Beneficiary','service','Barangays'));
    }

    public function search_availments(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        $service = $r->service;

        if($service = 'ched')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Ched_availments->where('name','like','%'.$r->search.'%')->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
                $Beneficiary->appends(['service' => $service]);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Ched_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'qfs')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Qfs_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('qfs_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Qfs_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('qfs_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'medical')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Medical_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('medical_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Medical_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('medical_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'financial')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Financial_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('financial_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Financial_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('financial_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'burial')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Burial_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('burial_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Burial_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('burial_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'dfa')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Dfa_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('dfa_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Dfa_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('dfa_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'pcso')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Pcso_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('pcso_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Pcso_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('pcso_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'legal')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Legal_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('legal_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Legal_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('legal_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'livelihood')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Gip_availments->where('name','like','%'.$r->search.'%')->orderBy('id')->whereNULL('gip_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Gip_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('gip_id')->where('status',1)->paginate(100);
            }
        }
        elseif($service = 'college')
        {
           if($r->searchoption == 'name')
            {
                $Beneficiary = $Ched_availments->where('fname','like','%'.$r->search.'%')->orwhere('mname','like','%'.$r->search.'%')->orwhere('lname','like','%'.$r->search.'%')->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
            }
            elseif($r->searchoption == 'address')
            {
                $Beneficiary = $Ched_availments->where('address','like','%'.$r->search.'%')->orderBy('id')->whereNULL('ched_id')->where('status',1)->paginate(100);
            }
        }

        return view('reverse/reverse', compact('Beneficiary','service'));

    }

    public function reverse_link(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;
        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Barangays = $Barangay->get();

        //dd($r->all());

        $availments = $r->availment_list;
        //dd($availments);
        $service = $r->service;

        if($r->service = 'ched')
        {
           $Beneficiary = $Ched->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
           $Availment_list = $Ched_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'qfs')
        {
            $Beneficiary = $Qfs->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Qfs_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'medical')
        {
            $Beneficiary = $Medical->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Medical_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'financial')
        {
            $Beneficiary = $Financial->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Financial_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'burial')
        {
            $Beneficiary = $Burial->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Burial_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'dfa')
        {
            $Beneficiary = $Dfa->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Dfa_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'pcso')
        {
            $Beneficiary = $Pcso->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Pcso_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'legal')
        {
            $Beneficiary = $Legal->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Legal_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'livelihood')
        {
            $Beneficiary = $Gip->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Gip_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }
        elseif($r->service = 'college')
        {
            $Beneficiary = $Ched->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Availment_list = $Ched_availments->whereIn('id',$r->availment_list)->orderBy('id')->where('status',1)->get();
        }

        return view('reverse/beneficiary', compact('Beneficiary','service','Availment_list','availments','Barangays'));
    }

    public function link_beneficiary(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        //dd($r->all());

        $availments = $r->availment_list;
        //dd($availments);
        $service = $r->service;

        if($r->service = 'ched')
        {
           $Beneficiary = $Ched->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
           $Ched_availments->whereIn('id',$r->availment_list)->update(['ched_id' => $r->beneficiary_id]);
           //dd($Availment_list);
           return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'qfs')
        {
            $Beneficiary = $Qfs->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Qfs_availments->whereIn('id',$r->availment_list)->update(['qfs_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'medical')
        {
            $Beneficiary = $Medical->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Medical_availments->whereIn('id',$r->availment_list)->update(['medical_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'financial')
        {
            $Beneficiary = $Financial->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Financial_availments->whereIn('id',$r->availment_list)->update(['financial_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'burial')
        {
            $Beneficiary = $Burial->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Burial_availments->whereIn('id',$r->availment_list)->update(['burial_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'dfa')
        {
            $Beneficiary = $Dfa->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Dfa_availments->whereIn('id',$r->availment_list)->update(['dfa_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'pcso')
        {
            $Beneficiary = $Pcso->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Pcso_availments->whereIn('id',$r->availment_list)->update(['pcso_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'legal')
        {
            $Beneficiary = $Legal->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Legal_availments->whereIn('id',$r->availment_list)->update(['legal_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'livelihood')
        {
            $Beneficiary = $Gip->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Gip_availments->whereIn('id',$r->availment_list)->update(['gip_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service = 'college')
        {
            $Beneficiary = $Ched->orderBy('id')->where('status',1)->with(['fetch_availments'])->get();
            $Ched_availments->whereIn('id',$r->availment_list)->update(['ched_id' => $r->beneficiary_id]);

            return Redirect()->route('reverse', compact('Beneficiary','service'));
        }

    }

    public function add_beneficiary_reverse(Request $r)
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;

        //dd($r->all());

        $availments = $r->availment_list;
        //dd($availments);
        $service = $r->service;

        if($r->service == 'ched')
        {
        $Ched->fname = !empty($r->fname)?$r->fname: ' ';
        $Ched->mname = !empty($r->mname)?$r->mname: ' ';
        $Ched->lname = !empty($r->lname)?$r->lname: ' ';
        $Ched->dob = date('Y-m-d', strtotime($r->dob));
        $Ched->address = !empty($r->address)?$r->address: ' ';
        $Ched->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Ched->district = !empty($r->district)?$r->district: ' ';
        $Ched->sex = !empty($r->sex)?$r->sex: ' ';
        $Ched->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched->email = !empty($r->email)?$r->email: ' ';
        $Ched->request_date = date('Y-m-d', strtotime($r->request_date));
        $Ched->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Ched->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Ched->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched->voters_status = 2;
        $Ched->save();

        $Ched_availments->whereIn('id',$r->availment_list)->update(['ched_id' => $Ched->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'qfs')
        {
        $Qfs->fname = !empty($r->fname)?$r->fname: ' ';
        $Qfs->mname = !empty($r->mname)?$r->mname: ' ';
        $Qfs->lname = !empty($r->lname)?$r->lname: ' ';
        $Qfs->dob = date('Y-m-d', strtotime($r->dob));
        $Qfs->address = !empty($r->address)?$r->address: ' ';
        $Qfs->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Qfs->district = !empty($r->district)?$r->district: ' ';
        $Qfs->sex = !empty($r->sex)?$r->sex: ' ';
        $Qfs->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Qfs->email = !empty($r->email)?$r->email: ' ';
        $Qfs->request_date = date('Y-m-d', strtotime($r->request_date));
        $Qfs->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Qfs->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Qfs->institution = !empty($r->institution)?$r->institution: ' ';
        $Qfs->amount = !empty($r->amount)?$r->amount: ' ';
        $Qfs->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Qfs->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Qfs->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Qfs->voters_status = 2;
        $Qfs->save();

        $Qfs_availments->whereIn('id',$r->availment_list)->update(['qfs_id' => $Qfs->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'medical')
        {
        $Medical->fname = !empty($r->fname)?$r->fname: ' ';
        $Medical->mname = !empty($r->mname)?$r->mname: ' ';
        $Medical->lname = !empty($r->lname)?$r->lname: ' ';
        $Medical->dob = date('Y-m-d', strtotime($r->dob));
        $Medical->address = !empty($r->address)?$r->address: ' ';
        $Medical->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Medical->district = !empty($r->district)?$r->district: ' ';
        $Medical->sex = !empty($r->sex)?$r->sex: ' ';
        $Medical->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Medical->email = !empty($r->email)?$r->email: ' ';
        $Medical->request_date = date('Y-m-d', strtotime($r->request_date));
        $Medical->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Medical->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Medical->institution = !empty($r->institution)?$r->institution: ' ';
        $Medical->amount = !empty($r->amount)?$r->amount: ' ';
        $Medical->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Medical->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Medical->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Medical->voters_status = 2;
        $Medical->save();

        $Medical_availments->whereIn('id',$r->availment_list)->update(['medical_id' => $Medical->id]);
        //$Beneficiary = $Financial->where('id',$r->beneficiary_id)->where('status',1)->with(['voters_info'])->first();

        //$Availments = $Financial_availments->where('ched_id',$r->beneficiary_id)->where('status',1)->get();

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'financial')
        {
        $Financial->fname = !empty($r->fname)?$r->fname: ' ';
        $Financial->mname = !empty($r->mname)?$r->mname: ' ';
        $Financial->lname = !empty($r->lname)?$r->lname: ' ';
        $Financial->dob = date('Y-m-d', strtotime($r->dob));
        $Financial->address = !empty($r->address)?$r->address: ' ';
        $Financial->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Financial->district = !empty($r->district)?$r->district: ' ';
        $Financial->sex = !empty($r->sex)?$r->sex: ' ';
        $Financial->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Financial->email = !empty($r->email)?$r->email: ' ';
        $Financial->request_date = date('Y-m-d', strtotime($r->request_date));
        $Financial->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Financial->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Financial->institution = !empty($r->institution)?$r->institution: ' ';
        $Financial->amount = !empty($r->amount)?$r->amount: ' ';
        $Financial->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Financial->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Financial->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Financial->voters_status = 2;
        $Financial->save();

        $Financial_availments->whereIn('id',$r->availment_list)->update(['financial_id' => $Financial->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'burial')
        {
        $Burial->fname = !empty($r->fname)?$r->fname: ' ';
        $Burial->mname = !empty($r->mname)?$r->mname: ' ';
        $Burial->lname = !empty($r->lname)?$r->lname: ' ';
        $Burial->dob = date('Y-m-d', strtotime($r->dob));
        $Burial->address = !empty($r->address)?$r->address: ' ';
        $Burial->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Burial->district = !empty($r->district)?$r->district: ' ';
        $Burial->sex = !empty($r->sex)?$r->sex: ' ';
        $Burial->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Burial->email = !empty($r->email)?$r->email: ' ';
        $Burial->request_date = date('Y-m-d', strtotime($r->request_date));
        $Burial->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Burial->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Burial->institution = !empty($r->institution)?$r->institution: ' ';
        $Burial->amount = !empty($r->amount)?$r->amount: ' ';
        $Burial->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Burial->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Burial->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Burial->voters_status = 2;
        $Burial->save();

        $Burial_availments->whereIn('id',$r->availment_list)->update(['burial_id' => $Burial->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'dfa')
        {
        $Dfa->fname = !empty($r->fname)?$r->fname: ' ';
        $Dfa->mname = !empty($r->mname)?$r->mname: ' ';
        $Dfa->lname = !empty($r->lname)?$r->lname: ' ';
        $Dfa->dob = date('Y-m-d', strtotime($r->dob));
        $Dfa->address = !empty($r->address)?$r->address: ' ';
        $Dfa->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Dfa->district = !empty($r->district)?$r->district: ' ';
        $Dfa->sex = !empty($r->sex)?$r->sex: ' ';
        $Dfa->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Dfa->email = !empty($r->email)?$r->email: ' ';
        $Dfa->request_date = date('Y-m-d', strtotime($r->request_date));
        $Dfa->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Dfa->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Dfa->institution = !empty($r->institution)?$r->institution: ' ';
        $Dfa->amount = !empty($r->amount)?$r->amount: ' ';
        $Dfa->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Dfa->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Dfa->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Dfa->voters_status = 2;
        $Dfa->save();

        $Dfa_availments->whereIn('id',$r->availment_list)->update(['dfa_id' => $Dfa->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'pcso')
        {
        $Pcso->fname = !empty($r->fname)?$r->fname: ' ';
        $Pcso->mname = !empty($r->mname)?$r->mname: ' ';
        $Pcso->lname = !empty($r->lname)?$r->lname: ' ';
        $Pcso->dob = date('Y-m-d', strtotime($r->dob));
        $Pcso->address = !empty($r->address)?$r->address: ' ';
        $Pcso->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Pcso->district = !empty($r->district)?$r->district: ' ';
        $Pcso->sex = !empty($r->sex)?$r->sex: ' ';
        $Pcso->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Pcso->email = !empty($r->email)?$r->email: ' ';
        $Pcso->request_date = date('Y-m-d', strtotime($r->request_date));
        $Pcso->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Pcso->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Pcso->institution = !empty($r->institution)?$r->institution: ' ';
        $Pcso->amount = !empty($r->amount)?$r->amount: ' ';
        $Pcso->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Pcso->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Pcso->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Pcso->voters_status = 2;
        $Pcso->save();

        $Pcso_availments->whereIn('id',$r->availment_list)->update(['pcso_id' => $Pcso->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'legal')
        {
        $Legal->fname = !empty($r->fname)?$r->fname: ' ';
        $Legal->mname = !empty($r->mname)?$r->mname: ' ';
        $Legal->lname = !empty($r->lname)?$r->lname: ' ';
        $Legal->dob = date('Y-m-d', strtotime($r->dob));
        $Legal->address = !empty($r->address)?$r->address: ' ';
        $Legal->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Legal->district = !empty($r->district)?$r->district: ' ';
        $Legal->sex = !empty($r->sex)?$r->sex: ' ';
        $Legal->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Legal->email = !empty($r->email)?$r->email: ' ';
        $Legal->request_date = date('Y-m-d', strtotime($r->request_date));
        $Legal->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Legal->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Legal->institution = !empty($r->institution)?$r->institution: ' ';
        $Legal->amount = !empty($r->amount)?$r->amount: ' ';
        $Legal->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Legal->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Legal->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Legal->voters_status = 2;
        $Legal->save();

        $Legal_availments->whereIn('id',$r->availment_list)->update(['legal_id' => $Legal->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'livelihood')
        {
        $Gip->fname = !empty($r->fname)?$r->fname: ' ';
        $Gip->mname = !empty($r->mname)?$r->mname: ' ';
        $Gip->lname = !empty($r->lname)?$r->lname: ' ';
        $Gip->dob = date('Y-m-d', strtotime($r->dob));
        $Gip->address = !empty($r->address)?$r->address: ' ';
        $Gip->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Gip->district = !empty($r->district)?$r->district: ' ';
        $Gip->sex = !empty($r->sex)?$r->sex: ' ';
        $Gip->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Gip->email = !empty($r->email)?$r->email: ' ';
        $Gip->request_date = date('Y-m-d', strtotime($r->request_date));
        $Gip->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Gip->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Gip->institution = !empty($r->institution)?$r->institution: ' ';
        $Gip->amount = !empty($r->amount)?$r->amount: ' ';
        $Gip->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Gip->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Gip->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Gip->voters_status = 2;
        $Gip->save();

        $Gip_availments->whereIn('id',$r->availment_list)->update(['gip_id' => $Gip->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }
        elseif($r->service == 'college')
        {
        $Ched->fname = !empty($r->fname)?$r->fname: ' ';
        $Ched->mname = !empty($r->mname)?$r->mname: ' ';
        $Ched->lname = !empty($r->lname)?$r->lname: ' ';
        $Ched->dob = date('Y-m-d', strtotime($r->dob));
        $Ched->address = !empty($r->address)?$r->address: ' ';
        $Ched->barangay_id = !empty($r->barangay_id)?$r->barangay_id: ' ';
        $Ched->district = !empty($r->district)?$r->district: ' ';
        $Ched->sex = !empty($r->sex)?$r->sex: ' ';
        $Ched->mobile_number = !empty($r->mobile_number)?$r->mobile_number: ' ';
        $Ched->email = !empty($r->email)?$r->email: ' ';
        $Ched->request_date = date('Y-m-d', strtotime($r->request_date));
        $Ched->service_type = !empty($r->service_type)?$r->service_type: ' ';
        $Ched->particulars = !empty($r->particulars)?$r->particulars: ' ';
        $Ched->institution = !empty($r->institution)?$r->institution: ' ';
        $Ched->amount = !empty($r->amount)?$r->amount: ' ';
        $Ched->action_officer = !empty($r->action_officer)?$r->action_officer: ' ';
        $Ched->recommendation = !empty($r->recommendation)?$r->recommendation: ' ';
        $Ched->remarks = !empty($r->remarks)?$r->remarks: ' ';
        $Ched->voters_status = 2;
        $Ched->save();

        $Ched_availments->whereIn('id',$r->availment_list)->update(['ched_id' => $Ched->id]);

        return Redirect()->route('reverse', compact('Beneficiary','service'));
        }

    }

}