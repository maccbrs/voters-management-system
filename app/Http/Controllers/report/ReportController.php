<?php

namespace QCRM\Http\Controllers\report;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use QCRM\Http\Controllers\Controller;
use QCRM\Exports\VotersExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function beneficiary_precinct($service)
    {

ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

        $Master_list = [];

        if($service == 'all'):
            $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');

            $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());
        elseif($service == 'ched'):
            $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');

            $Master_list = $Ched_Barangay->toArray();
        elseif($service == 'dfa'):
            $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');

            $Master_list = $Dfa_Barangay->toArray();
        elseif($service == 'financial'):
            $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Financial_Barangay->toArray();
        elseif($service == 'medical'):
            $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Medical_Barangay->toArray();
        elseif($service == 'pcso'):
            $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Pcso_Barangay->toArray();
        elseif($service == 'burial'):
            $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Burial_Barangay->toArray();
        elseif($service == 'qfs'):
            $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Qfs_Barangay->toArray();
        elseif($service == 'gip'):
            $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Gip_Barangay->toArray();
        elseif($service == 'legal'):            
            $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Legal_Barangay->toArray();
        else:
            $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',' ')->with(['voters_info'])->get()->groupBy('voters_info.precint');

            $Master_list = $Ched_Barangay->toArray();
        endif;

        return view('report/report', compact('Master_list','service'));
    }

    public function beneficiary_barangay($service)
    {

        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

        $Master_list = [];

        if($service == 'all'):
            $Ched_Barangay = $Ched->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Dfa_Barangay = $Dfa->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Financial_Barangay = $Financial->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Medical_Barangay = $Medical->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Pcso_Barangay = $Pcso->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->with(['fetch_barangay'])->where('barangay','!=','')->get()->groupBy('fetch_barangay.barangay');
            $Burial_Barangay = $Burial->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Qfs_Barangay = $Qfs->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Gip_Barangay = $Gip->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Legal_Barangay = $Legal->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');


            $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());
        elseif($service == 'ched'):
            $Ched_Barangay = $Ched->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        elseif($service == 'dfa'):
            $Dfa_Barangay = $Dfa->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Dfa_Barangay->toArray();
        elseif($service == 'financial'):
            $Financial_Barangay = $Financial->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Financial_Barangay->toArray();
        elseif($service == 'medical'):
            $Medical_Barangay = $Medical->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Medical_Barangay->toArray();
        elseif($service == 'pcso'):
            $Pcso_Barangay = $Pcso->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->with(['fetch_barangay'])->where('barangay','!=','')->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Pcso_Barangay->toArray();
        elseif($service == 'burial'):
            $Burial_Barangay = $Burial->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Burial_Barangay->toArray();
        elseif($service == 'qfs'):
            $Qfs_Barangay = $Qfs->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Qfs_Barangay->toArray();
        elseif($service == 'gip'):
            $Gip_Barangay = $Gip->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Gip_Barangay->toArray();
        elseif($service == 'legal'):
            $Legal_Barangay = $Legal->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Legal_Barangay->toArray();
        else:
            $Ched_Barangay = $Ched->whereNotNull('barangay_id')->where('barangay_id','!=',' ')->where('barangay','!=',' ')->where('barangay','!=','')->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        endif;

        return view('report/report2', compact('Master_list','service'));


    }

    public function voter_precinct()
    {
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Voter = new \QCRM\Models\Voters;

        $Master_list = $Voter->select('name', 'address', 'district','precint')->get()->groupBy('precint');

        return view('report/report3', compact('Master_list'));
    }

    public function beneficiary_street($service)
    {
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        $Streets = new \QCRM\Models\Streets;

        $Streets_list = $Streets->select('street')->get()->toArray();
        //dd($Streets_list);

        $Master_list = [];

        if($service == 'all'):
        $Ched_Barangay = $Ched->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        //dd($Ched_Barangay);
        $Dfa_Barangay = $Dfa->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        //dd($Dfa_Barangay);
        $Financial_Barangay = $Financial->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        //dd($Financial_Barangay);
        $Medical_Barangay = $Medical->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        //dd($Medical_Barangay);
        $Pcso_Barangay = $Pcso->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Burial_Barangay = $Burial->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Qfs_Barangay = $Qfs->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Gip_Barangay = $Gip->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Legal_Barangay = $Legal->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');

        $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());

        elseif ($service == 'ched'):
        $Ched_Barangay = $Ched->where(function ($query) use($Streets_list) {
         for ($i = 0; $i < count($Streets_list); $i++){
            $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
         }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');

        $Master_list = $Ched_Barangay->toArray();
        elseif ($service == 'qfs'):
        $Qfs_Barangay = $Qfs->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Qfs_Barangay->toArray();
        
        elseif ($service == 'medical'):
        $Medical_Barangay = $Medical->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Medical_Barangay->toArray();
        
        elseif ($service == 'financial'):
        $Financial_Barangay = $Financial->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Financial_Barangay->toArray();
        
        elseif ($service == 'burial'):
        $Burial_Barangay = $Burial->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Burial_Barangay->toArray();
        
        elseif ($service == 'dfa'):
        $Dfa_Barangay = $Dfa->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Dfa_Barangay->toArray();
        
        elseif ($service == 'pcso'):
        $Pcso_Barangay = $Pcso->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Pcso_Barangay->toArray();
        
        elseif ($service == 'legal'):
        $Legal_Barangay = $Legal->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Legal_Barangay->toArray();
        
        elseif ($service == 'livelihood'):
        $Gip_Barangay = $Gip->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Gip_Barangay->toArray();
        
        elseif ($service == 'college'):
        $Ched_Barangay = $Ched->where(function ($query) use($Streets_list) {
         for ($i = 0; $i < count($Streets_list); $i++){
            $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
         }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Master_list = $Ched_Barangay->toArray();
        
        endif;

        return view('report/report4', compact('Master_list','service'));
    }

    public function qfamily_street($service)
    {

        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        $Streets = new \QCRM\Models\Streets;

        $Streets_list = $Streets->select('street')->get()->toArray();

        $Master_list = [];

        if($service == 'all'):
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');

        $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());
        elseif($service == 'ched'):
            $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        elseif($service == 'dfa'):
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Dfa_Barangay->toArray();
        elseif($service == 'financial'):
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Financial_Barangay->toArray();
        elseif($service == 'medical'):
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Medical_Barangay->toArray();
        elseif($service == 'pcso'):
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Pcso_Barangay->toArray();
        elseif($service == 'burial'):
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Burial_Barangay->toArray();
        elseif($service == 'qfs'):
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Qfs_Barangay->toArray();
        elseif($service == 'gip'):
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Gip_Barangay->toArray();
        elseif($service == 'legal'):
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Legal_Barangay->toArray();
        else:
            $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->where(function ($query) use($Streets_list) {
             for ($i = 0; $i < count($Streets_list); $i++){
                $query->orwhere('address', 'like',  '%' . explode(' ', $Streets_list[$i]['street'])[0] .'%');
             }      
        })->with(['fetch_barangay'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        endif;

        // dd($Master_list);
        return view('report/qfamily_street', compact('Master_list', 'service'));
    }

    public function qfamily_barangay($service)
    {

        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        $Streets = new \QCRM\Models\Streets;

        if($service == 'all'):
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');

        $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());
        elseif($service == 'ched'):
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        elseif($service == 'dfa'):
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Dfa_Barangay->toArray();
        elseif($service == 'financial'):
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Financial_Barangay->toArray();
        elseif($service == 'medical'):
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Medical_Barangay->toArray();
        elseif($service == 'pcso'):
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Pcso_Barangay->toArray();
        elseif($service == 'burial'):
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Burial_Barangay->toArray();
        elseif($service == 'qfs'):
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Qfs_Barangay->toArray();
        elseif($service == 'gip'):
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Gip_Barangay->toArray();
        elseif($service == 'legal'):
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Legal_Barangay->toArray();
        else:
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('fetch_barangay.barangay');
            $Master_list = $Ched_Barangay->toArray();
        endif;

        return view('report/qfamily_barangay', compact('Master_list','service'));
    }

    public function qfamily_precinct($service)
    {

        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;
        $Streets = new \QCRM\Models\Streets;

        if($service == 'all'):
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');

        $Master_list = array_merge($Ched_Barangay->toArray(),$Qfs_Barangay->toArray(),$Dfa_Barangay->toArray(),$Legal_Barangay->toArray(),$Gip_Barangay->toArray(),$Burial_Barangay->toArray(),$Financial_Barangay->toArray(),$Medical_Barangay->toArray(),$Pcso_Barangay->toArray());
        elseif($service == 'ched'):
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'] )->get()->groupBy('voters_info.precint');
            $Master_list = $Ched_Barangay->toArray();
        elseif($service == 'dfa'):
        $Dfa_Barangay = $Dfa->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Dfa_Barangay->toArray();
        elseif($service == 'financial'):
        $Financial_Barangay = $Financial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Financial_Barangay->toArray();
        elseif($service == 'medical'):
        $Medical_Barangay = $Medical->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Medical_Barangay->toArray();
        elseif($service == 'pcso'):
        $Pcso_Barangay = $Pcso->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Pcso_Barangay->toArray();
        elseif($service == 'burial'):
        $Burial_Barangay = $Burial->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Burial_Barangay->toArray();
        elseif($service == 'qfs'):
        $Qfs_Barangay = $Qfs->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Master_list = $Qfs_Barangay->toArray();
        elseif($service == 'gip'):
        $Gip_Barangay = $Gip->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
        $Master_list = $Gip_Barangay->toArray();
        elseif($service == 'legal'):            
        $Legal_Barangay = $Legal->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Legal_Barangay->toArray();
        else:
        $Ched_Barangay = $Ched->whereNotNull('voters_id')->where('voters_id','!=',0)->with(['fetch_barangay','fetch_street','voters_info'])->get()->groupBy('voters_info.precint');
            $Master_list = $Ched_Barangay->toArray();
        endif;

        return view('report/qfamily_precinct', compact('Master_list','service'));
    }

    public function add_query(Request $r)
    {
        $Reports_query = new \QCRM\Models\Report; 

        $Reports_query->name = $r->name;
        $Reports_query->query = $r->query;
        $Reports_query->save();

        return Redirect::back()->with('message', 'Success');
    }

    public function generate_report(Request $r)
    {
        $Reports_query = new \QCRM\Models\report\Report; 
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

        $sample_report = $Ched->whereNotNull('voters_id')->with(['voters_info'])->get();

        return view('report/generated_report', compact('sample_report'));
    }

    public function sample_report(Request $r)
    {
        $Reports_query = new \QCRM\Models\report\Report; 

        $Selected = $Reports_query->where('id',$r->id)->first();

        $Generated = DB::select($Selected->query)->get();

        return view('report/generated_report', compact('Generated'));
    }

    public function export() 
    {
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', -1);

        return Excel::download(new VotersExport, 'voters.ods');
    }
}   
