<?php

namespace QCRM\Http\Controllers\admin;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;

class usermanagementController extends Controller
{
    public function index()
    {
    	$UsersModel = new \QCRM\Models\User;

    	$Users = $UsersModel->get();

        return view('admin/user_management', compact('Users'));
    }

    public function resetpassword(Request $r)
    {
    	$UsersModel = new \QCRM\Models\User;

    	$UsersModel
    	->where('id',$r->hidden_id)
    	->update(['password' => bcrypt($r->password)]);

    	$Users = $UsersModel
    	->where('id',$r->hidden_id)
    	->first();
    
    	return redirect()->route('usermanagement')->with('message', $Users->name."'s".' password has been updated.');
    }

    public function change_access(Request $r)
    {
        $UsersModel = new \QCRM\Models\User;

        $UsersModel
        ->where('id',$r->hidden_id)
        ->update(['access' => $r->access]);
        //dd($r->all());

        $Users = $UsersModel
        ->where('id',$r->hidden_id)
        ->first();

        return redirect()->route('usermanagement')->with('message', $Users->name."'s".' access has been updated.');
    }

    public function add_user(Request $r)
    {
        $UsersModel = new \QCRM\Models\User;

        $UsersModel->name = $r->name;
        $UsersModel->email = $r->email;
        $UsersModel->password = bcrypt($r->password);
        $UsersModel->access = 1;
        $UsersModel->save();

        $Users = $UsersModel
        ->where('id',$UsersModel->id)
        ->first();


        return redirect()->route('usermanagement')->with('message', $Users->name."'s".' access has been created.');
    }


public function announcement()
    {

        return view('admin/announcement');
    }

    public function post_announce(Request $r)
    {
        $Announcement = new \QCRM\Models\Announcements;

        $Announcement->content = $r->content;
        $Announcement->save();

        return view('admin/announcement');
    }

public function utility()
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;


ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);
        $Availment_list = $Voter->get();
        //dd(count($Availment_list));

$i = 0;

        foreach($Availment_list as $a)
        {
            $i++;
                $Burial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Ched
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Dfa
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Financial
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Gip
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Legal
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Medical
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Pcso
                ->where('fname',$a['fname'])->where('lname',$a['lname'])
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);

                $Qfs
                ->where('name','like','%'.$a['fname'].'%')->where('name','like','%'.$a['lname'].'%')
                    ->update(
                    [
                            'voters_id' => $a['id'],
                    ]);


        }

        
        //dd($Burial_list);

        return $i;



    }

    public function utility_2()
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

        $Ched_Beneficiary = $Ched->get();
        foreach($Ched_Beneficiary as $cb)
        {
            $Ched_availments->where('name','like','%'.$cb['fname'].'%')->where('name','like','%'.$cb['lname'].'%')
                    ->update(
                    [
                            'ched_id' => $cb['id'],
                    ]);
        }

        $Dfa_Beneficiary = $Dfa->get();
        foreach($Dfa_Beneficiary as $db)
        {
            $Dfa_availments->where('fname',$db['fname'])->where('lname',$db['lname'])
                    ->update(
                    [
                            'dfa_id' => $db['id'],
                    ]);
        }

        $Financial_Beneficiary = $Financial->get();
        foreach($Financial_Beneficiary as $fb)
        {
            $Financial_availments->where('fname',$fb['fname'])->where('lname',$fb['lname'])
                    ->update(
                    [
                            'financial_id' => $fb['id'],
                    ]);
        }

        $Medical_Beneficiary = $Medical->get();
        foreach($Medical_Beneficiary as $mb)
        {
            $Medical_availments->where('fname',$mb['fname'])->where('lname',$mb['lname'])
                    ->update(
                    [
                            'medical_id' => $mb['id'],
                    ]);
        }

        $Pcso_Beneficiary = $Pcso->get();
        foreach($Pcso_Beneficiary as $pb)
        {
            $Pcso_availments->where('fname',$pb['fname'])->where('lname',$pb['lname'])
                    ->update(
                    [
                            'pcso_id' => $pb['id'],
                    ]);
        }   

        $Burial_Beneficiary = $Burial->get();
        foreach($Burial_Beneficiary as $bb)
        {
            $Burial_availments->where('fname',$bb['fname'])->where('lname',$bb['lname'])
                    ->update(
                    [
                            'burial_id' => $bb['id'],
                    ]);
        }  

        $Qfs_Beneficiary = $Qfs->get();
        foreach($Qfs_Beneficiary as $qb)
        {
            $Qfs_availments->where('name','like','%'.$qb['name'].'%')
                    ->update(
                    [
                            'qfs_id' => $qb['id'],
                    ]);
        }  
            
        return 'success';     
    }

    public function utility_3()
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);
        $Availment_list = $Voter->get();
        //dd(count($Availment_list));

$i = 0;

        foreach($Availment_list as $a)
        {
            $i++;
                $Burial
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Ched
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Dfa
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Financial
                 ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Gip
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Legal
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Medical
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Pcso
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);

                $Qfs
                ->where('voters_id',0)
                    ->update(
                    [
                            'voters_status' => 0,
                    ]);


        }

        
        //dd($Burial_list);

        return $i;
    }

    public function utility_4()
    {
ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $BeneficiaryModel = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voter = new \QCRM\Models\Voters;

        $Financial_availments_list = $Medical_availments->whereNull('medical_id')->get()->toArray();
        //dd($Ched_availments_list);
        //dd($Ched_availments_list);

        foreach($Financial_availments_list as $c)
        {

/*            if (strpos($c['name'], ',') !== false)
            {
            $lastname = explode(',', $c['name'])[0];
            $firstname = trim(explode(',', $c['name'])[1]);
            }
            else
            {
            $lastname = explode(' ', $c['name'])[0];
            $firstname = trim(explode(' ', $c['name'])[1]);
            }*/

            $lastname = $c['fname'];
            $firstname = $c['lname'];

            $Barangay = new \QCRM\Models\beneficiary\Barangay;
            //dd($firstname);

            $Financial_master = $Medical->where('lname',$lastname)->where('fname','like','%'.$firstname.'%')->first();
            $Barangay_list = $Barangay->where('barangay',$c['barangay'])->first();

            if(empty($Financial_master))
            {
                $Financial_save = new \QCRM\Models\beneficiary\Medical;

                $Financial_save->fname = $firstname;
                $Financial_save->lname = $lastname;
                $Financial_save->address = !empty($c['address'])?$c['address']: NULL;
                $Financial_save->barangay_id = !empty($Barangay_list->id)?$Barangay_list->id: NULL;
                $Financial_save->mobile_number = !empty($c['contact_no'])?$c['contact_no']: NULL;
                $Financial_save->email = !empty($c['email'])?$c['email']: NULL;
                $Financial_save->save();

                $Medical_availments->where('id',$c['id'])
                ->update(['medical_id' => $Financial_save->id]);
            }
            else
            {
                $Medical_availments->where('id',$c['id'])
                ->update(['medical_id' => $Financial_master->id]);
            }
/*
            $Ched_master = $Ched->where('');*/
/*            $Ched_availments->where('name','like','%'.$cb['fname'].'%')->where('name','like','%'.$cb['lname'].'%')
                    ->update(
                    [
                            'ched_id' => $cb['id'],
                    ]);*/
        }

            
        return 'success';     
    }

    public function utility_5()
    {
ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Voters_list = $Voter->get()->toArray();
        //dd($Ched_availments_list);
        //dd($Ched_availments_list);

        foreach($Voters_list as $v)
        {
            $explode = explode("-",$v['comelec_id_no']);
            //dd($explode[1]);

            $Voter->where('id',$v['id'])->update(['precint' => $explode[1]]);
        }


            
        return 'success';     
    }

    public function utility_6()
    {
ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        //dd($Ched_availments_list);
        //dd($Ched_availments_list);

        $Ched_list = $Ched->get();
        $Dfa_list = $Dfa->get();
        $Financial_list = $Financial->get();
        $Medical_list = $Medical->get();
        $Pcso_list = $Pcso->get();
        $Burial_list = $Burial->get();
        $Qfs_list = $Qfs->get();
        $Gip_list = $Gip->get();
        $Legal_list = $Legal->get();

        foreach($Ched_list as $c)
        {
            //dd($c);
            $bar = $c->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            //dd($Barangay_id);

            $List = $Ched->where('id',$c->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Dfa_list as $d)
        {
            $bar = $d->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Dfa->where('id',$d->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Financial_list as $f)
        {
            $bar = $f->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Financial->where('id',$f->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Medical_list as $m)
        {
            $bar = $m->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Medical->where('id',$m->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Pcso_list as $p)
        {
            $bar = $p->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Pcso->where('id',$p->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Burial_list as $b)
        {
            $bar = $b->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Burial->where('id',$b->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Qfs_list as $q)
        {
            $bar = $q->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Qfs->where('id',$q->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Gip_list as $g)
        {
            $bar = $g->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Gip->where('id',$g->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }

        foreach($Legal_list as $l)
        {
            $bar = $l->barangay;
            $Barangay_id = $Barangay->where('barangay','like','%'.$bar.'%')->first();

            $List = $Legal->where('id',$l->id)->update(['barangay_id' => !empty($Barangay_id->id)?$Barangay_id->id: NULL]);
        }
            
        return 'success';     
    }

    public function utility_7()
    {
ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        //dd($Ched_availments_list);
        //dd($Ched_availments_list);

        foreach($Barangay->get() as $b)
        {
            $Ched->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Dfa->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Financial->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Medical->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Pcso->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Burial->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Qfs->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Gip->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            $Legal->where('address','like','%'.$b->barangay.'%')->update(['barangay' => $b->barangay, 'barangay_id' => $b->id]);
            //dd($explode[1]);

            //$Voter->where('id',$v['id'])->update(['precint' => $explode[1]]);
        }


            
        return 'success';     
    }

    public function utility_8()
    {
ini_set('memory_limit',-1);
ini_set('max_execution_time', -1);

        $Barangay = new \QCRM\Models\beneficiary\Barangay;

        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Streets = new \QCRM\Models\Streets;
        //dd($Ched_availments_list);
        //dd($Ched_availments_list);

        $Street = $Streets->get();

        foreach($Street as $b)
        {
            $Ched->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Dfa->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Financial->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Medical->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Pcso->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Burial->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Qfs->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Gip->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            $Legal->where('address','like','%'.$b->street.'%')->where('barangay_id',$b->barangay_id)->update(['street_id' => $b->id]);
            //dd($explode[1]);

            //$Voter->where('id',$v['id'])->update(['precint' => $explode[1]]);
        }


            
        return 'success';     
    }

}   
