<?php

namespace QCRM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        ini_set('memory_limit','-1');
        $UsersModel = new \QCRM\Models\User;
        $Master = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;
        $Announcements = new \QCRM\Models\Announcements;

        $Ched_latest = $Ched->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Dfa_latest = $Dfa->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Burial_latest = $Burial->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Financial_latest = $Financial->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Medical_latest = $Medical->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Pcso_latest = $Pcso->orderBy('request_date','desc')->where('status',1)->paginate(10);

        $Announcement_latest = $Announcements->orderBy('id','desc')->first();

        $Ched_barangay = DB::table('ched')
                     ->select(DB::raw('count(*) as total'), 'ched.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Qfs_barangay = DB::table('qfs')
                     ->select(DB::raw('count(*) as total'), 'qfs.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Dfa_barangay = DB::table('dfa')
                     ->select(DB::raw('count(*) as total'), 'dfa.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Legal_barangay = DB::table('legal')
                     ->select(DB::raw('count(*) as total'), 'legal.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Gip_barangay = DB::table('gip')
                     ->select(DB::raw('count(*) as total'), 'gip.address')
                     ->groupBy('address')
                     ->orderBy('total','desc')
                     ->get();

        $College_barangay = DB::table('ched')
                     ->select(DB::raw('count(*) as total'), 'ched.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Burial_barangay = DB::table('burial')
                     ->select(DB::raw('count(*) as total'), 'burial.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Financial_barangay = DB::table('financial')
                     ->select(DB::raw('count(*) as total'), 'financial.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Medical_barangay = DB::table('medical')
                     ->select(DB::raw('count(*) as total'), 'medical.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Pcso_barangay = DB::table('pcso')
                     ->select(DB::raw('count(*) as total'), 'pcso.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Master_list = array_merge($Ched_barangay->toArray(),$Qfs_barangay->toArray(),$Dfa_barangay->toArray(),$Legal_barangay->toArray(),$Gip_barangay->toArray(),$College_barangay->toArray(),$Burial_barangay->toArray(),$Dfa_barangay->toArray(),$Financial_barangay->toArray(),$Medical_barangay->toArray(),$Pcso_barangay->toArray());

                 //dd($Master_list);
        //dd($Announcement_latest);

        $Users = $UsersModel->get();
        /*$Beneficiary = $Master->orderBy('request_date','desc')->where('status',1)->paginate(10);*/


        $Burial_master_count = $Burial->count();
        $Ched_master_count = $Ched->count();
        $Dfa_master_count = $Dfa->count();
        $Financial_master_count = $Financial->count();
        $Medical_master_count = $Medical->count();
        $Pcso_master_count = $Pcso->count();
        $Qfs_master_count = $Qfs->count();
        $Legal_master_count = $Legal->count();
        $Gip_master_count = $Gip->count();
        $College_master_count = $Ched->count();


        $Burial_count = $Burial_availments->count();
        $Ched_count = $Ched_availments->count();
        $Dfa_count = $Dfa_availments->count();
        $Financial_count = $Financial_availments->count();
        $Medical_count = $Medical_availments->count();
        $Pcso_count = $Pcso_availments->count();
        $Qfs_count = $Qfs_availments->count();
        $Legal_count = $Legal_availments->count();
        $Gip_count = $Gip_availments->count();
        $College_count = $Ched_availments->count();


        $Master_count = $Burial_master_count + $Ched_master_count + $Dfa_master_count + $Financial_master_count + $Medical_master_count + $Pcso_master_count + $Qfs_master_count + $Legal_master_count + $Gip_master_count + $College_master_count;

        $Availment_count = $Burial_count + $Ched_count + $Dfa_count + $Financial_count + $Medical_count + $Pcso_count + $Qfs_count + $Legal_count + $Gip_count + $College_count;

        $Voters_count = $Voters->count();

        //dd($Availment_count);

        return view('home', compact('Users','Availment_count','Master_count','Ched_latest','Burial_latest','Dfa_latest','Financial_latest','Medical_latest','Pcso_latest','Announcement_latest','Master_list','Ched_barangay','Burial_barangay','Dfa_barangay','Financial_barangay','Medical_barangay','Pcso_barangay','Qfs_barangay','Legal_barangay','Gip_barangay','College_barangay','Voters_count'));
    }



/*    public function parser()
    {
        $BeneficiaryModel = new \QCRM\Models\beneficiary\SocialServicesController;
        $Med_trans = new \QCRM\Models\beneficiary\MedTransactions;

        $Test = $BeneficiaryModel->get()->toArray();

        foreach($Test as $t)
        {
            explode($t->services_types)
            foreach($t->)
        }

        
    }*/


    public function charts()
    {
        ini_set('memory_limit','-1');
        $UsersModel = new \QCRM\Models\User;
        $Master = new \QCRM\Models\beneficiary\Master;
        $Ched = new \QCRM\Models\beneficiary\Ched;
        $Ched_availments = new \QCRM\Models\beneficiary\Ched_availments;
        $Dfa = new \QCRM\Models\beneficiary\Dfa;
        $Dfa_availments = new \QCRM\Models\beneficiary\Dfa_availments;
        $Financial = new \QCRM\Models\beneficiary\Financial;
        $Financial_availments = new \QCRM\Models\beneficiary\Financial_availments;
        $Medical = new \QCRM\Models\beneficiary\Medical;
        $Medical_availments = new \QCRM\Models\beneficiary\Medical_availments;
        $Pcso = new \QCRM\Models\beneficiary\Pcso;
        $Pcso_availments = new \QCRM\Models\beneficiary\Pcso_availments;
        $Burial = new \QCRM\Models\beneficiary\Burial;
        $Burial_availments = new \QCRM\Models\beneficiary\Burial_availments;
        $Qfs = new \QCRM\Models\beneficiary\Qfs;
        $Qfs_availments = new \QCRM\Models\beneficiary\Qfs_availments;
        $Gip = new \QCRM\Models\beneficiary\Gip;
        $Gip_availments = new \QCRM\Models\beneficiary\Gip_availments;
        $Legal = new \QCRM\Models\beneficiary\Legal;
        $Legal_availments = new \QCRM\Models\beneficiary\Legal_availments;
        $Voters = new \QCRM\Models\Voters;
        $Announcements = new \QCRM\Models\Announcements;

        $Ched_latest = $Ched->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Dfa_latest = $Dfa->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Burial_latest = $Burial->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Financial_latest = $Financial->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Medical_latest = $Medical->orderBy('request_date','desc')->where('status',1)->paginate(10);
        $Pcso_latest = $Pcso->orderBy('request_date','desc')->where('status',1)->paginate(10);

        $Announcement_latest = $Announcements->orderBy('id','desc')->first();

        $Ched_barangay = DB::table('ched')
                     ->select(DB::raw('count(*) as total'), 'ched.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Qfs_barangay = DB::table('qfs')
                     ->select(DB::raw('count(*) as total'), 'qfs.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Dfa_barangay = DB::table('dfa')
                     ->select(DB::raw('count(*) as total'), 'dfa.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Legal_barangay = DB::table('legal')
                     ->select(DB::raw('count(*) as total'), 'legal.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

                     //dd($Legal_barangay);

        $Gip_barangay = DB::table('gip')
                     ->select(DB::raw('count(*) as total'), 'gip.address')
                     ->groupBy('address')
                     ->orderBy('total','desc')
                     ->get();

        $College_barangay = DB::table('ched')
                     ->select(DB::raw('count(*) as total'), 'ched.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Burial_barangay = DB::table('burial')
                     ->select(DB::raw('count(*) as total'), 'burial.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Financial_barangay = DB::table('financial')
                     ->select(DB::raw('count(*) as total'), 'financial.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Medical_barangay = DB::table('medical')
                     ->select(DB::raw('count(*) as total'), 'medical.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        $Pcso_barangay = DB::table('pcso')
                     ->select(DB::raw('count(*) as total'), 'pcso.barangay')
                     ->groupBy('barangay')
                     ->orderBy('total','desc')
                     ->get();

        return view('layouts/charts', compact('Users','Beneficiary','Availment_count','Master_count','Ched_latest','Burial_latest','Dfa_latest','Financial_latest','Medical_latest','Pcso_latest','Announcement_latest','Master_list','Ched_barangay','Burial_barangay','Dfa_barangay','Financial_barangay','Medical_barangay','Pcso_barangay','Qfs_barangay','Legal_barangay','Gip_barangay','College_barangay','Voters_count'));             
    }
}
