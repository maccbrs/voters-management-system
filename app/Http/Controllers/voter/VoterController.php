<?php

namespace QCRM\Http\Controllers\voter;

use Illuminate\Http\Request;
use QCRM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use QCRM\Exports\VotersExport;
use Maatwebsite\Excel\Facades\Excel;

class VoterController extends Controller
{
    public function index()
    {
        $Voters = new \QCRM\Models\Voters;

    	$Voter_list = $Voters->where('status',1)->paginate(50);

    	return view('voter/voters', compact('Voter_list'));
    }

    public function export()
    {
        return Excel::download(new VotersExport(), 'Voters.csv');
    }

    public function search(Request $r)
    {
        $Voters = new \QCRM\Models\Voters;

        $searchoption = $r->searchoption;
        $search = $r->search;

        if($searchoption == 'name'):
            $Voter_list = $Voters->where('name','like','%'.$search.'%')->where('status',1)->paginate(50);
        elseif($searchoption == 'address'):
            $Voter_list = $Voters->where('address','like','%'.$search.'%')->where('status',1)->paginate(50);
        endif;
        //dd($search);

        return view('voter/voters', compact('Voter_list'));
    }

    public function tag(Request $r)
    {
        $Voters = new \QCRM\Models\Voters;

        $Voter_tag = $Voters
                    ->where('id',$r->id)
                    ->update(['voter_tag' => $r->options]);
    }

    public function voter_info(Request $r)
    {
        $Voters = new \QCRM\Models\Voters;

        $Voter_list = $Voters->where('id', $r->id)->with(['burial_beneficiaries','ched_beneficiaries','dfa_beneficiaries','financial_beneficiaries','gip_beneficiaries','legal_beneficiaries','medical_beneficiaries','pcso_beneficiaries','qfs_beneficiaries'])->first();

        return view('voter/view', compact('Voter_list'));
    }

}