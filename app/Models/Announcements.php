<?php

namespace QCRM\Models;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'announcement';   
    protected $fillable = ['content'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
