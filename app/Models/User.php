<?php

namespace QCRM\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'users';   
    protected $fillable = ['name','email'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
