<?php

namespace QCRM\Models;

use Illuminate\Database\Eloquent\Model;

class Voters extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'voters_list';   
    protected $fillable = ['name','voters_status'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function burial_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Burial','voters_id','id')->with(['fetch_availments']);
   }

   public function ched_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Ched','voters_id','id')->with(['fetch_availments']);
   }

   public function dfa_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Dfa','voters_id','id')->with(['fetch_availments']);
   }

   public function financial_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Financial','voters_id','id')->with(['fetch_availments']);
   }

   public function gip_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Gip','voters_id','id')->with(['fetch_availments']);
   }

   public function legal_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Legal','voters_id','id')->with(['fetch_availments']);
   }

   public function medical_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Medical','voters_id','id')->with(['fetch_availments']);
   }

   public function pcso_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Pcso','voters_id','id')->with(['fetch_availments']);
   }

   public function qfs_beneficiaries(){
        return $this->hasOne('QCRM\Models\beneficiary\Qfs','voters_id','id')->with(['fetch_availments']);
   }
}
