<?php

namespace QCRM\Models\report;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $connection = 'mysql';
    protected $table = 'report_query';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
    

}