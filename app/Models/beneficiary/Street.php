<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $connection = 'mysql';
    protected $table = 'street';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

}