<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Gip_availments extends Model
{
    protected $connection = 'mysql';
    protected $table = 'gip_availments';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function fetch_officer(){
    return $this->hasOne('QCRM\Models\User','id','action_officer');
   }

/*   public function voters_info(){
        return $this->hasOne('QCRM\Models\Voters','id','voters_id');
   }
*/
/*   public function financial_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Financial_availments','financial_id','id');
   }

   public function total_financial_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Financial_availments','financial_id','id')->count();
   }*/
}