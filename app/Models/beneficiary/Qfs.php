<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Qfs extends Model
{
    protected $connection = 'mysql';
    protected $table = 'qfs';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function voters_info(){
        return $this->hasOne('QCRM\Models\Voters','id','voters_id');
   }

   public function fetch_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Qfs_availments','qfs_id','id')->where('status',1);
   }

   public function fetch_barangay(){
      return $this->hasOne('QCRM\Models\beneficiary\Barangay','id','barangay_id');
   }

   public function fetch_street(){
      return $this->hasOne('QCRM\Models\Streets','id','street_id');
   }
}