<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    protected $connection = 'mysql';
    protected $table = 'financial';   
    protected $fillable = ['fname','mname','lname','dob','address','barangay','district','sex','mobile_number','email','request_date','service_type','particulars','institution','amount','service_satus','action_officer','recommendation','remarks'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function voters_info(){
        return $this->hasOne('QCRM\Models\Voters','id','voters_id');
   }
   public function fetch_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Financial_availments','financial_id','id')->where('status',1);
   }
   public function fetch_barangay(){
      return $this->hasOne('QCRM\Models\beneficiary\Barangay','id','barangay_id');
   }

   public function fetch_street(){
      return $this->hasOne('QCRM\Models\Streets','id','street_id');
   }

}