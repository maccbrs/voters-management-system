<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Ched_availments extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ched_availments';   
    protected $fillable = ['date','code','name','award_no','birthday','gender','address','barangay','school','course','year_level','amount','contact_no','contact_no_2','landline','email','parent','batch','school_yr','sem','status','ched_id'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function fetch_officer(){
	  return $this->hasOne('QCRM\Models\User','id','action_officer');
   }
}