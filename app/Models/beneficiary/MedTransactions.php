<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class MedTransactions extends Model
{
    protected $connection = 'mysql';
    protected $table = 'test_med_trans';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
