<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Legal_availments extends Model
{
    protected $connection = 'mysql';
    protected $table = 'legal_availments';   
    protected $fillable = ['fname','mname','lname','dob','address','barangay','district','sex','mobile_number','email','request_date','service_type','particulars','institution','amount','service_satus','action_officer','recommendation','remarks'];
    protected $hidden = [
        'password', 'remember_token',
    ];

   public function fetch_officer(){
    return $this->hasOne('QCRM\Models\User','id','action_officer');
   }
/*   public function voters_info(){
        return $this->hasOne('QCRM\Models\Voters','id','voters_id');
   }*/

/*   public function pcso_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Pcso_availments','burial_id','id');
   }

   public function total_pcso_availments(){
        return $this->hasMany('QCRM\Models\beneficiary\Pcso_availments','burial_id','id')->count();
   }*/
}