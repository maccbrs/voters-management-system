<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
    protected $connection = 'mysql';
    protected $table = 'barangay';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

}