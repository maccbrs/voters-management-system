<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class SocialServices extends Model
{
    protected $connection = 'mysql';
    protected $table = 'test_med_2';   
    protected $fillable = [];
    protected $hidden = [
        'password', 'remember_token',
    ];


   public function voters_info(){
        return $this->hasOne('QCRM\Models\Voters','id','voters_id');
   }
}
