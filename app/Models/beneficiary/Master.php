<?php

namespace QCRM\Models\beneficiary;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $connection = 'mysql';
    protected $table = 'master';   
    protected $fillable = ['fname','mname','lname','dob','address','barangay','district','sex','mobile_number','email','request_date','service_type','particulars','institution','amount','service_satus','action_officer','recommendation','remarks'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}