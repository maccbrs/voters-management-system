<?php

namespace QCRM\Models;

use Illuminate\Database\Eloquent\Model;

class Streets extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'street';   
    protected $fillable = ['barangay','street','village'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
