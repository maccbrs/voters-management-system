<?php

namespace QCRM\Exports;

use QCRM\Models\Voters;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VotersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Voters::select('comelec_id_no','name','lname','mname','address','birthdate','gender','district','precint')->orderBy('precint', 'asc')->get();
    }

    public function headings(): array
    {
        return [
				'Comelec ID #',
				'Name',
				'LastName',
				'FirstName',
				'Address',
				'BirthDate',
				'Gender',
				'District',
				'Precinct'
        ];
    }


}
