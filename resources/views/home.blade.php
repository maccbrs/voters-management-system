@extends('layouts.app')

@section('pageTitle', 'Home')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')
<div class="col-md-12">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

<!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-users-cog"></i>
                  </div>
                  <div class="mr-5"><h5>{{ $Users->count() }}</h5> Registered Users</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="{{ url('/user_management') }}">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-users"></i>
                  </div>
                  <div class="mr-5"><h5>{{ $Master_count }}</h5> Registered Beneficiaries</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="{{ url('/search_all') }}">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-cogs"></i>
                  </div>
                  <div class="mr-5"><h5>{{ $Availment_count }}</h5> Registered Availments</h5></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="#">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-life-ring"></i>
                  </div>
                  <div class="mr-5"><h5>{{ $Voters_count }}</h5> Registered Voters</h5></div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="{{ url('/voters') }}">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card text-white bg-dark mb-3">
                <div class="card-header"><i class="fas fa-exclamation-circle"></i> Announcements</div>
                  <div class="card-body">
                      <p class="card-text">{!! $Announcement_latest->content !!}</p>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent Scholarship Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#scholarship">Show</button></div>
                  <div id="scholarship" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Ched_latest))
                        @foreach($Ched_latest as $c)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$c->id, 'service'=>'ched']) }}">{{strtoupper($c->lname.', '.$c->fname.' '.$c->mname)}}</a></td>
                          <td>{{ !empty($c->address)?$c->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent DFA Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#dfa">Show</button></div>
                  <div id="dfa" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Dfa_latest))
                        @foreach($Dfa_latest as $d)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$d->id, 'service'=>'dfa']) }}">{{strtoupper($d->lname.', '.$d->fname.' '.$d->mname)}}</a></td>
                          <td>{{ !empty($d->address)?$d->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent Burial Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#burial">Show</button></div>
                  <div id="burial" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Burial_latest))
                        @foreach($Burial_latest as $b)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$b->id, 'service'=>'burial']) }}">{{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}</a></td>
                          <td>{{ !empty($b->address)?$b->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent Financial Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#financial">Show</button></div>
                  <div id="financial" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Financial_latest))
                        @foreach($Financial_latest as $f)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$f->id, 'service'=>'financial']) }}">{{strtoupper($f->lname.', '.$f->fname.' '.$f->mname)}}</a></td>
                          <td>{{ !empty($f->address)?$f->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent Medical Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#medical">Show</button></div>
                  <div id="medical" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Medical_latest))
                        @foreach($Medical_latest as $m)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$m->id, 'service'=>'medical']) }}">{{strtoupper($m->lname.', '.$m->fname.' '.$m->mname)}}</a></td>
                          <td>{{ !empty($m->address)?$m->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header" data-toggle="collapse"><i class="fas fa-folder-open"></i> Recent PCSO Grantees <button class="btn btn-sm btn-info float-right" data-toggle="collapse" data-target="#pcso">Show</button></div>
                  <div id="pcso" class="collapse card-body">
                    <table class="table table-sm table-hover">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Pcso_latest))
                        @foreach($Pcso_latest as $p)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$p->id, 'service'=>'pcso']) }}">{{strtoupper($p->lname.', '.$d->fname.' '.$p->mname)}}</a></td>
                          <td>{{ !empty($p->address)?$p->address: 'NO DATA' }}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
</script>

@endsection