<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Q-CRM</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('bootstrap-4.1.3/css/bootstrap.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .col-centered{
                float: none;
                margin: 0 auto;
            }

            .page-footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <?php /* <a href="{{ route('register') }}">Register</a> */ ?>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <b>Q-CRM System</b>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <p>This is the Q-CRM System, the Constituent Relationship Management System for the Office of Representative Romero Federico Quimbo.</p>
                    </div>
                </div>

                @auth
                <div class="row">
                    <div class="col-md-12 col-centered">
                        <div class="alert alert-success font-weight-bold" role="alert">
                          Logged In. Please proceed to <a href="{{ url('/home') }}"><i class="fas fa-home"></i> Dashboard</a>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-12 col-centered">
                        <div class="alert alert-warning" role="alert">
                          Please <u><b><a href="{{ route('login') }}">login</a></b></u> with your credentials to access.
                        </div>
                    </div>
                </div>
                @endauth

                <!-- Footer -->
                <footer class="page-footer blue">
                  <!-- Copyright -->
                  <div class="footer-copyright text-center py-3"><small>© 2015-2018. Office of Rep. Romero Federico Quimbo, House of Representatives, Republic of the Philippines. All Rights Reserved. | 
                    <a href="#"> QCRM.com</a></small>
                  </div>
                  <!-- Copyright -->

                </footer>
                <!-- Footer -->

            </div>
        </div>
    </body>
</html>
