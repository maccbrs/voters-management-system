@extends('layouts.app')

@section('pageTitle', 'Link')

@section('header-style')
<style type="text/css">
.align-items-center {
  -ms-flex-align: center!important;
  align-items: center!important;
}
.d-flex {
  display: -ms-flexbox!important;
  display: flex!important;
}
</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

<div class="container h-100">
    <div class="row align-items-center h-100 justify-content-center">
        <div class="col-md-12 text-center" id="link">


        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
$(document).ready(function() {
$('#link').html('<img src={{ asset("logo/processing.gif") }}>');
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    
        document.getElementById("link").innerHTML = xhttp.responseText;

    }
  };
@if($status == 'new')
xhttp.open("GET", "{{route('link_voter')}}?status=new", true);
@elseif($status == 'voter')
xhttp.open("GET", "{{route('link_voter')}}?status=voter", true);
@elseif($status == 'nonvoter')
xhttp.open("GET", "{{route('link_voter')}}?status=new", true);
@endif
  xhttp.send();
  });
</script>

@endsection