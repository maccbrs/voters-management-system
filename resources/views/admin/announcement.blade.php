@extends('layouts.app')

@section('pageTitle', 'Announcement')

@section('content')


<div class="col-md-12">
  <div class="container">
     <form id="announce" action="{{ URL('/post_announce') }}" method="POST">
         {{ csrf_field() }}
    <div class="card">
      <div class="card-header"><h3>Announcement</h3></div>
      <div class="card-body">
        <textarea name="content" id="editor">
          
        </textarea>
      </div>
      <div class="card-footer"> 
        <button type="submit" class="btn btn-primary float-right">Submit</button>
    </form>
    </div>
    </div>
  </div>
</div>


@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    $('#usermanagement').DataTable();
} );

    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
@endsection