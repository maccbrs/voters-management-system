@extends('layouts.app')

@section('pageTitle', 'User Management')

@section('content')

<style type="text/css">
  th.dt-center, td.dt-center { text-align: center; }
</style>

<div class="modal fade" id="adduser" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ url('/add_user')}}" class="form-horizontal p-10">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="password">Name:</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Fullname">
          </div>
          <div class="form-group">
            <label for="password">Email:</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

 <!-- The Modal -->
  <div class="modal fade" id="resetpassword">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Password</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form method="POST" action="{{ url('/reset')}}" class="form-horizontal p-10">
        {{ csrf_field() }}
          <input type="hidden" id="hidden_id" name="hidden_id" value="">
          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          </div>
          </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
        </div>
        
      </div>
    </div>
  </div>

 <!-- The Modal -->
  <div class="modal fade" id="changeaccess">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Access</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form method="POST" action="{{ url('/change_access')}}" class="form-horizontal p-10">
        {{ csrf_field() }}
          <input type="hidden" id="hidden_id_access" name="hidden_id" value="">
            <div class="form-group">
              <label for="sel1">Select Access:</label>
              <select class="form-control" name="access" id="sel1">
                <option value="0">User</option>
                <option value="1">Admin</option>
              </select>
            </div>
          </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
        </div>
        
      </div>
    </div>
  </div>



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

        @if(empty(session()->has('message')))
        <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
          <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

            <div class="card">
                <div class="card-header"><i class="fas fa-columns"></i> Users Management</div>

                <div class="card-body">

                @if(session()->has('message'))
                   <div id="message" class="fading-alert">
                       <div class="alert alert-success">
                          <i class="far fa-check-circle"></i> {{ session()->get('message') }}
                       </div>
                   </div>
                @endif
                <div class="row">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#adduser" style="margin-bottom: 15px;"><i class="fas fa-plus"> Add User</i></button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-hover table-sm" id="usermanagement">
                        <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Tools</th>

                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($Users))
                        @foreach($Users as $u)
                        <tr>
                            <td>{{$u->name}}</td>
                            <td>{{$u->email}}</td>
                            <td>
                              <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal"  onclick="$('#hidden_id').val('{{$u->id}}');" data-target="#resetpassword"><i class="fas fa-key" aria-hidden="true"></i> Password</button>
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal"  onclick="$('#hidden_id_access').val('{{$u->id}}');" data-target="#changeaccess"><i class="fas fa-toolbox" aria-hidden="true"></i> Access</button>
                                <button type="button" class="btn btn-sm btn-danger"><i class="fas fa-minus" aria-hidden="true"></i> Disable</button>
                              </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                  </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    $('#usermanagement').DataTable({
      "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],});
} );
</script>
@endsection