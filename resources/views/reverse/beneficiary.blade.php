@extends('layouts.app')

@section('pageTitle', 'View')

@section('subPage', 'Availment')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>
 <div class="modal fade" id="add_beneficiary">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title">Add Beneficiary</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!-- Modal body -->
                              <div class="modal-body">
                                
                                <div class="row">

                                <div class="col-md-12">
                                <form action="{{ URL('/add_beneficiary_reverse') }}" method="POST" autocomplete="off">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="service" value="ched">
                                  


                                  <div class="form-group custom_bottom">
                                  <label for="fname" class="custom_bottom">First Name</label>
                                  <input class="form-control form-control-sm" id="fname" name="fname" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="mname" class="custom_bottom">Middle Name</label>
                                  <input class="form-control form-control-sm" id="mname" name="mname" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="lname" class="custom_bottom">Last Name</label>
                                  <input class="form-control form-control-sm" id="lname" name="lname" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="dob" class="custom_bottom">Date of Birth</label>
                                  <input class="form-control form-control-sm" id="dob" name="dob" type="date" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="address" class="custom_bottom">Address</label>
                                  <input class="form-control form-control-sm" id="address" name="address" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="barangay" class="custom_bottom">Barangay</label>
                                    <select class="form-control" id="barangay" name="barangay_id">
                                      <option disabled selected>* Please Select Barangay</option>
                                      @foreach($Barangays as $barangay)
                                      <option value="{{$barangay->id}}">{{$barangay->barangay}}</option>
                                      @endforeach
                                    </select>
                                  </div>

<!--                                   <div class="form-group custom_bottom">
                                  <label for="district" class="custom_bottom">District</label>
                                  <input class="form-control form-control-sm" id="district" name="district" type="text" value="">
                                  </div> -->

                                  <div class="form-group custom_bottom">
                                  <label for="sex" class="custom_bottom">Sex</label>
                                  <input class="form-control form-control-sm" id="sex" name="sex" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="mobile_number" class="custom_bottom">Mobile Number</label>
                                  <input class="form-control form-control-sm" id="mobile_number" name="mobile_number" type="text" value="">
                                  </div>

                                  <div class="form-group custom_bottom">
                                  <label for="email" class="custom_bottom">Email</label>
                                  <input class="form-control form-control-sm" id="email" name="email" type="email" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="request_date" class="custom_bottom">Request Date</label>
                                  <input class="form-control form-control-sm" id="request_date" name="request_date" type="date" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="service_type" class="custom_bottom">Service Type</label>
                                  <input class="form-control form-control-sm" id="service_type" name="service_type" type="text" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="particulars" class="custom_bottom">Particulars</label>
                                  <input class="form-control form-control-sm" id="particulars" name="particulars" type="text" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="Institution" class="custom_bottom">Institution</label>
                                  <input class="form-control form-control-sm" id="Institution" name="Institution" type="text" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="amount" class="custom_bottom">Amount</label>
                                  <input class="form-control form-control-sm" id="amount" name="amount" type="number" value="">
                                </div>

                                  <input class="form-control form-control-sm" id="action_officer" name="action_officer" type="hidden" value="{{ !empty(Auth::user()->id)?Auth::user()->id: '' }}">

                                <div class="form-group custom_bottom">
                                  <label for="recommendation" class="custom_bottom">Recommendations</label>
                                  <textarea class="form-control form-control-sm" id="recommendation" name="recommendation"></textarea>
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="remarks" class="custom_bottom">Remarks</label>
                                  <input class="form-control form-control-sm" id="remarks" name="remarks" type="text" value="">
                                </div>


                              </div>


                              </div>

                            </div>
                              
                              <!-- Modal footer -->
                              <div class="modal-footer">

                        @foreach($Availment_list as $a)
                        <input type="hidden" name="availment_list[]" value="{{$a->id}}">
                        @endforeach
                        <input type="hidden" name="service" value="{{$service}}">

                                <button type="submit" class="btn btn-primary">Submit</button>
                                                      </form>
                              </div>
                              
                            </div>
                          </div>
                        </div>




<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> Selected Availments</div>
                <div class="card-body">
                  <div class="container">
                    <div class="btn-group">
<!--                       <button type="button" class="btn btn-success btn-sm" style="margin-bottom: 10px;" data-toggle="modal" data-target="#add_beneficiary"><i class="fas fa-plus"> Add Beneficiary</i></button> -->

                    </div>
                    <table class="table table-sm">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Birthdate</th>
                          <th>Address</th>
                          <th>Barangay</th>
                          <th>District</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Availment_list))
                        @foreach($Availment_list as $a)
                        <tr>
                          @if($service == 'ched')
                          <td>{{strtoupper($a->name)}}</td>
                          @else
                          <td>{{strtoupper($a->lname.', '.$a->fname.' '.$a->mname)}}</td>
                          @endif
                          <td>{{!empty($a->dob)?$a->dob: 'No Data'}}</td>
                          <td>{{!empty($a->address)?$a->address: 'No Data'}}</td>
                          <td>{{!empty($a->barangay)?$a->barangay: 'No Data'}}</td>
                          <td>{{!empty($a->district)?$a->district: 'No Data'}}</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>



            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> {{strtoupper($service)}} Beneficiary List</div>
                <div class="card-body">
                  <div class="container">
                    <div class="btn-group">
                    </div>
                      <form action="{{ URL('/link_beneficiary') }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" name="service" value="{{$service}}">
                        @foreach($Availment_list as $a)
                        <input type="hidden" name="availment_list[]" value="{{$a->id}}">
                        @endforeach
                    <table class="table table-sm" id="beneficiary_search">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                          <th>Amount</th>
                          <th>Status</th>
                          <th>Link</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Beneficiary))
                        @foreach($Beneficiary as $b)
                        <tr>
                          @if($service == 'gip')
                          <td>{{strtoupper($b->name)}}</td>
                          @else
                          <td>{{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}</td>
                          @endif
                          <td>{{!empty($b->address)?$b->address: 'No Data'}}</td>
                          <td>{{$b->fetch_availments->sum('amount')}}</td>
                          <td>@if($b->status == '1') Active @else Inactive @endif</td>
                            <td>
                                <button type="submit" class="btn btn-primary btn-sm" name="beneficiary_id" value="{{$b->id}}"> Link</i></button></td>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    <button type="button" class="btn btn-successs btn-sm float-right" data-toggle="modal" data-target="#add_beneficiary"><i class="fas fa-plus"> Add Beneficiary</i></button>
                  </form>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>




        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#beneficiary_search').DataTable({
    "ordering": false
  });
} );
</script>

@endsection