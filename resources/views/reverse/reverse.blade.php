@extends('layouts.app')

@section('pageTitle', 'Availment')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

                            <!-- The Modal -->
                       

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if(!empty($msg))
              <div class="alert alert-success alert-dismissible fade show fading-alert"> {{ $msg }}</div>
            @endif

<!--             <form method="POST" action="{{ url('/search_availments')}}" class="form-horizontal p-10">
                {{ csrf_field() }}
                <input type="hidden" name="service" value="{{$service}}">
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="search" placeholder="Search">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Search</button>
              </div>
            </div>
            <div class="container">
              <div class="row">
            <small>Search Options:</small>    
            <div class="radio col-md-2">
              <label>
                <input type="radio" name="searchoption" id="optionsRadios1" value="name" checked>
                Name
              </label>
            </div>
            <div class="radio col-md-2">
              <label>
                <input type="radio" name="searchoption" id="optionsRadios2" value="address">
                Address
              </label>
            </div>
            </div>
            </div>
            </form> -->


            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> {{strtoupper($service)}} Availment List</div>
                <div class="card-body">
                  <div class="container">



                    <div class="container">


                    </div>


                      <form action="{{ URL('/reverse_link') }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                      <input type="hidden" name="service" value="{{$service}}">
                      <div class="row">
                        @if(count($Beneficiary))
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary btn-sm float-right" data-toggle="modal" style="margin-bottom: 15px;"><i class="fas fa-edit"> Link Beneficiary</i></button>
                        </div>
                        @endif
                      </div>
                    <table class="table table-sm" id="beneficiary_search">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Recipient</th>
                          <th>Address</th>
                          <th>Amount</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Beneficiary))
                        @foreach($Beneficiary as $b)
                        <tr>
                          <td>
                            <div class="form-check">
                              <input type="checkbox" class="form-check-input" name="availment_list[]" value="{{$b->id}}">
                            </div>
                          </td>
                          @if($service == 'ched' or $service == 'qfs' or $service == 'college')
                          <td>{{strtoupper($b->name)}}</td>
                          @else
                          <td>{{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}</td>
                          @endif
                          <td>{{!empty($b->address)?$b->address: 'No Data'}}</td>
                          <td>{{!empty($b->amount)?$b->amount: '0'}}</td>
                          <td>@if($b->status == '1') Active @else Inactive @endif</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>

                    {{ $Beneficiary->appends(request()->input())->links() }}
                  </div>
                      </form>

                </div>
              </div>
            </div>
          </div>
          




        </div>
    </div>
</div>
</div>

@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#beneficiary_search').DataTable({
    "ordering": false
  });
} );
</script>

@endsection