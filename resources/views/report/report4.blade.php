@extends('layouts.app')

@section('pageTitle', 'Report')

@section('content')
        <div class="col-md-12">

        @if(empty(session()->has('message')))
        <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
          <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

          <div class="row">
            <div class="col-md-3">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Report Filter</div>
                  <div class="card-body">
                    <div class="row">

                      <div class="col-md-6">
                        <div class="btn-group">
                          <button class="btn btn-secondary btn-sm" type="button" disabled>
                            Services
                          </button>
                          <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'all']) }}">All Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'ched']) }}">Ched Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'qfs']) }}">QFS Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'medical']) }}">Medical Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'financial']) }}">Financial Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'burial']) }}">Burial Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'dfa']) }}">DFA Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'pcso']) }}">PCSO Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'legal']) }}">Legal Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'livelihood']) }}">Livelihood Assistance</a>
                            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'college']) }}">College Assistance</a>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <p><b>Viewing:</b></p>
                        <small>
                          <mark>
                          @if($service = 'all')All Assistance
                          @elseif($service = 'ched')Ched Assistance
                          @elseif($service = 'qfs')QFS Assistance
                          @elseif($service = 'medical')Medical Assistance
                          @elseif($service = 'financial')Financial Assistance
                          @elseif($service = 'burial')Burial Assistance
                          @elseif($service = 'dfa')DFA Assistance
                          @elseif($service = 'pcso')PCSO Assistance
                          @elseif($service = 'legal')Legal Assistance
                          @elseif($service = 'livelihood')Livelihood Assistance
                          @else College Assistance
                          @endif
                          </mark>
                        </small>
                      </div>

                    </div>
                  </div>
              </div>
            </div>

            <div class="col-md-9">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Street Report Graph</div>
                  <div class="card-body">
                  <div id="barangay"></div>
                  </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Street Report</div>
                  <div class="card-body">
                    <table class="table table-sm table-hover" id="report">
                      <thead>
                        <tr>
                          <th>Street</th>
                          <th>Count</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>District</th>
                          <th>Zip Code</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Master_list))
                        @php
                        $count = 0;
                        @endphp

                        @foreach($Master_list as $m => $l)
                        @php
                        $count+= count($l);
                        @endphp

                        @if(!empty($l))
                        @php
                        $i = 0;
                        @endphp
                        @foreach($l as $v)
                        <tr>
                          @php
                          $i++;
                          @endphp
                          <td>@if($i == 1){{!empty($m)?$m: 'N/A'}}@endif</td>
                          <td>@if($i == 1){{count($l)}}@endif</td>
                          <td>
                            <small>@if(!empty($v['name'])){{ $v['name'] }}@else{{ $v['lname'] }}, {{ $v['fname'] }}@if($v['mname'] == 'NO DATA')@else {{ $v['mname'] }}@endif @endif</small>
                          </td>
                          <td>
                            <small>{{!empty($v['address'])?$v['address']: 'N/A'}}</small>
                          </td>
                          <td>
                            <small>{{!empty($v['fetch_barangay']['district'])?$v['fetch_barangay']['district']: 'N/A'}}</small>
                          </td>
                          <td>
                            <small>{{!empty($v['fetch_barangay']['zip_code'])?$v['fetch_barangay']['zip_code']: 'N/A'}}</small>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                      <tfoot>
                          <tr>
                              <th></th>
                              <th>Total: {{$count}}</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                          </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
          </div>

          </div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    var table = $('#report').DataTable({
            ordering: false,
            fixedHeader: {
                header: true,
                footer: true
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

        table.buttons().container()
        .appendTo( '#report_wrapper .col-md-6:eq(0)' );
} );
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  
  function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

</script>

      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

      <script>
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Street', 'Street', { role: 'style' }],
                @if(!empty($Master_list))
                @foreach($Master_list as $k => $v)
               ['{{!empty($k)?$k: "N/A"}}', {{sizeof($v)}}, getRandomColor()],
                @endforeach
                @endif
            ]);

            var options = {title: 'Street Report'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('barangay'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

@endsection