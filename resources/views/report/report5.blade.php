@extends('layouts.app')

@section('pageTitle', 'Report')

@section('content')
        <div class="col-md-12">

        @if(empty(session()->has('message')))
        <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
          <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

  <div class="modal fade" id="add_query">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Access</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form method="POST" action="{{ url('/change_access')}}" class="form-horizontal p-10">
        {{ csrf_field() }}
            
            <div class="form-group">
              <label for="exampleFormControlInput1">Name</label>
              <input type="text" class="form-control" id="exampleFormControlInput1">
            </div>

            <div class="form-group">
              <label for="exampleFormControlTextarea1">Query</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
        </div>
        
      </div>
    </div>
  </div>

<!--           <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Q Families Report Graph</div>
                  <div class="card-body">
                  <div id="barangay"></div>
                  </div>
              </div>
            </div>
          </div> -->

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Q Families Report</div>
                  <div class="card-body">
                    <table class="table table-sm table-hover" id="report">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Street</th>
                          <th>District</th>
                          <th>Barangay</th>
                          <th>Precinct</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Master_list))
                        @php
                        $count = 0;
                        @endphp

                        @foreach($Master_list as $m => $l)
                        @php
                        $count+= count($l);
                        @endphp

                        @if(!empty($l))
                        @php
                        $i = 0;
                        @endphp
                        @foreach($l as $v)
                        <tr>
                          @php
                          $i++;
                          @endphp
                          <td>
                            @if(!empty($l['name'])){{ $l['name'] }}@else{{ $l['lname'] }}, {{ $l['fname'] }}@if($l['mname'] == 'NO DATA')@else{{ $l['mname'] }}@endif @endif
                          </td>
                          <td>
                            {{!empty($l['fetch_street']['street'])?$l['fetch_street']['street']: ''}}
                          </td>
                          <td>
                            {{$l['fetch_barangay']['district']}}
                          </td>
                          <td>
                            {{$l['fetch_barangay']['barangay']}}
                          </td>
                          <td>
                            {{$l['voters_info']['precint']}}
                          </td>
                        </tr>
                        @endforeach
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    var table = $('#report').DataTable({
            ordering: false,
            fixedHeader: {
                header: true,
                footer: true
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
        ]
    });

        table.buttons().container()
        .appendTo( '#report_wrapper .col-md-6:eq(0)' );
} );
</script>
<!-- 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  
  function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

</script>

      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

      <script>
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
                @if(!empty($Master_list))
                @foreach($Master_list as $k => $v)
               ['{{$k}}', {{sizeof($v)}}, getRandomColor()],
                @endforeach
                @endif
            ]);

            var options = {title: 'Barangay Report'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('barangay'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script> -->

@endsection