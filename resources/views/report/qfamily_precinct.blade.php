@extends('layouts.app')

@section('pageTitle', 'Report')

@section('content')
        <div class="col-md-12">

        @if(empty(session()->has('message')))
        <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
          <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

          <div class="row">
            <div class="col-md-3">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Report Filter</div>
                  <div class="card-body">
                    <div class="row">

                      <div class="col-md-6">
                        <div class="btn-group">
                          <button class="btn btn-secondary btn-sm" type="button" disabled>
                            Services
                          </button>
                          <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'all']) }}">All Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'ched']) }}">Ched Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'qfs']) }}">QFS Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'medical']) }}">Medical Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'financial']) }}">Financial Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'burial']) }}">Burial Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'dfa']) }}">DFA Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'pcso']) }}">PCSO Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'legal']) }}">Legal Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'livelihood']) }}">Livelihood Assistance</a>
                            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'college']) }}">College Assistance</a>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <p><b>Viewing:</b></p>
                        <small>
                          <mark>
                          @if($service = 'all')All Assistance
                          @elseif($service = 'ched')Ched Assistance
                          @elseif($service = 'qfs')QFS Assistance
                          @elseif($service = 'medical')Medical Assistance
                          @elseif($service = 'financial')Financial Assistance
                          @elseif($service = 'burial')Burial Assistance
                          @elseif($service = 'dfa')DFA Assistance
                          @elseif($service = 'pcso')PCSO Assistance
                          @elseif($service = 'legal')Legal Assistance
                          @elseif($service = 'livelihood')Livelihood Assistance
                          @else College Assistance
                          @endif
                          </mark>
                        </small>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

            <div class="col-md-9">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Precint Report Graph</div>
                  <div class="card-body">
                  <div id="precint"></div>
                  </div>
              </div>
            </div>
            
          </div>

          <div id='png'></div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Precint Report</div>
                  <div class="card-body">
                    <table class="table table-sm table-hover" id="report">
                      <thead>
                        <tr>
                          <th>Precint</th>
                          <th>Count</th>
                          <th>Comelec ID #</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Gender</th>
                          <th>District</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Master_list))
                        @php
                        $count = 0;
                        @endphp

                        @foreach($Master_list as $m => $l)
                        @php
                        $count+= count($l);
                        @endphp

                        @if(!empty($l))
                        @php
                        $i = 0;
                        @endphp
                        @foreach($l as $v)
                        <tr>
                          @php
                          $i++;
                          @endphp
                          <td>@if($i == 1){{!empty($m)?$m: 'N/A'}}@endif</td>
                          <td>@if($i == 1){{count($l)}}@endif</td>
                          <td>
                            <small>{{!empty($v['voters_info']['comelec_id_no'])?$v['voters_info']['comelec_id_no']: 'N/A'}}</small>
                          </td>
                          <td>
                            <small>@if(!empty($v['name'])){{ $v['name'] }}@else{{ $v['lname'] }}, {{ $v['fname'] }}@if($v['mname'] == 'NO DATA')@else {{ $v['mname'] }}@endif @endif</small>
                          </td>
                          <td>
                            <small>{{!empty($v['voters_info']['address'])?$v['voters_info']['address']: 'N/A'}}</small>
                          </td>
                          <td>
                            <small>{{!empty($v['voters_info']['gender'])?$v['voters_info']['gender']: 'N/A'}}</small>
                          </td>
                          <td>
                            <small>{{!empty($v['voters_info']['district'])?$v['voters_info']['district']: 'N/A'}}</small>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                        @endforeach
                        @endif
                      </tbody>
                      <tfoot>
                          <tr>
                              <th></th>
                              <th>Total: {{$count}}</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                          </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    var table = $('#report').DataTable({
              ordering: false,
            fixedHeader: {
                header: true,
                footer: true
            },
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

        table.buttons().container()
        .appendTo( '#report_wrapper .col-md-6:eq(0)' );
} );
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  
  function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

</script>

      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

      <script>
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Precint', 'Precint', { role: 'style' }],
                @if(!empty($Master_list))
                @foreach($Master_list as $k => $v)
               ['{{$k}}', {{sizeof($v)}}, getRandomColor()],
                @endforeach
                @endif
            ]);

            var options = {title: 'Precint Report'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('precint'));

/*            google.visualization.events.addListener(chart, 'ready', function () {
              document.getElementById('precint').innerHTML = '<img src="' + chart.getImageURI() + '">';
              console.log(document.getElementById('precint').innerHTML);
            });
            */
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

@endsection