@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

        @if(empty(session()->has('message')))
        <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
          <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

            <div class="card">
                <div class="card-header"><i class="fas fa-columns"></i> Generated Report</div>

                <div class="card-body">

                    <table class="table table-hover table-sm" id="usermanagement">
                        <thead class="thead-light">
                            <tr>
                                <th>Barangay</th>
                                <th># of Beneficiaries</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($Generated))
                        @foreach($Generated as $g)
                        <tr>
                            <td>{{$g->barangay}}</td>
                            <td>{{$g->total}}</td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>

                @if(session()->has('message'))
                   <div id="message" class="fading-alert">
                       <div class="alert alert-success">
                          <i class="far fa-check-circle"></i> {{ session()->get('message') }}
                       </div>
                   </div>
                @endif

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(1000, function(){
    $(".fading-alert").slideUp(1000);
});

    $(document).ready(function() {
    $('#report').DataTable();
} );
</script>
@endsection