@extends('layouts.app')

@section('pageTitle', 'Search Voter')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')
<div class="modal modal-fade" id="confirm_link" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

        <div class="row">

          <div class="col-md-8">
            <div class="card bg-info" style="margin-bottom: 20px;">
              <div class="card-header text-white">Beneficiary Info</div>
                <div class="card-body bg-light">

                  <div class="container">
                    <div class="row">
                      <div class="col-md-3">
                        <p style="margin-bottom: 0px;">Firstname: </p>
                        <p style="margin-bottom: 0px;">Middlename: </p>
                        <p style="margin-bottom: 0px;">Last Name: </p>
                        <p style="margin-bottom: 0px;">Address: </p>
                      </div>
                      <div class="col-md-3">
                        <p style="margin-bottom: 0px;"><b>{{!empty($beneficiary_fname)?$beneficiary_fname: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($beneficiary_mname)?$beneficiary_mname: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($beneficiary_lname)?$beneficiary_lname: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($beneficiary_address)?$beneficiary_address: 'No Data'}}</b></p>
                      </div>
                    </div>
                  </div>

                </div> 
            </div>
          </div>

          <div class="col-md-4">
            <div class="card bg-info" style="margin-bottom: 20px;">
              <div class="card-header text-white">Non-Voter</div>
                <div class="card-body bg-light">

                  <div class="container text-center">
                    <form id="link" action="{{ URL('/non_voter') }}" method="post">
                      {{ csrf_field() }}
                        <input type="hidden" name="service" value="{{$service}}">
                        <input type="hidden" value="{{$beneficiary_id}}" name="beneficiary_id">
                        <button type="submit" class="btn btn-danger"><i class="fas fa-user-times"></i> Tag as Non-Voter</button>
                    </form>
                  </div>

                </div> 
            </div>
          </div>

        </div>

            <div class="card" style="margin-bottom: 15px;">
              <div class="card-body">
                      <form method="get" action="{{ url('/search_voter')}}" class="form-horizontal p-10" autocomplete="off">
                          {{ csrf_field() }}
                          <input type="hidden" value="{{$beneficiary_id}}" name="beneficiary_id">
                          <input id="fname" type="hidden" name="fname" value="{{$beneficiary_fname}}">
                          <input id="mname" type="hidden" name="mname" value="{{$beneficiary_mname}}">
                          <input id="lname" type="hidden" name="lname" value="{{$beneficiary_lname}}">
                          <input id="address" type="hidden" name="address" value="{{$beneficiary_address}}">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                      </div>
                      <div class="container">
                        

                      <div class="row">
                        <div class="col-md-2">
                          <p>Search Options:</p>
                        </div>
                        <div class="col-md-2">
                          <div class="btn-group btn-group-sm btn-group-toggle float-left" data-toggle="buttons">
                            <label class="btn btn-info btn-sm active">
                              <input type="radio" name="searchoption" id="option1" autocomplete="off" value="name" checked> Name
                            </label>
                            <label class="btn btn-info btn-sm">
                              <input type="radio" name="searchoption" id="option2" autocomplete="off" value="address"> Address
                            </label>
                          </div>
                        </div>
                      </div>

                      </div>
                      </form>
              </div>
            </div>

            <div class="row">

            <div class="container">
              <small><b><a href="{{ URL::previous() }}">< Back</a></b></small>
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> Filtered Voters</div>
                <div class="card-body">
                  <div class="container">
                    <table class="table table-hover table-sm" id="voter_search">
                      <thead class="thead-light">
                        <tr>
                          <th>ID No</th>
                          <th>Comelec ID No</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Birthdate</th>
                          <th>Gender</th>
                          <th>District</th>
                          <th>Precint</th>
                          <th>Link</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Voters))
                        @foreach($Voters as $v)
                        <tr>
                          <td>{{!empty($v->id_no)?$v->id_no: 'No Data'}}</td>
                          <td>{{!empty($v->comelec_id_no)?$v->comelec_id_no: 'No Data'}}</td>
                          <td>{{!empty($v->name)?strtoupper($v->name): 'No Data'}}</a></td>
                          <td>{{!empty($v->address)?$v->address: 'No Data'}}</td>
                          <td>{{!empty($v->birthdate)?$v->birthdate: 'No Data'}}</td>
                          <td>{{!empty($v->gender)?$v->gender: 'No Data'}}</td>
                          <td>{{!empty($v->district)?$v->district: 'No Data'}}</td>
                          <td>{{!empty($v->precint)?$v->preint: 'No Data'}}</td>
                          <td>
                            <form id="link" action="{{ URL('/link_voter') }}" method="post">
                              {{ csrf_field() }}
                              <input type="hidden" name="service" value="{{$service}}">
                              <input type="hidden" value="{{$v->id}}" name="voters_id">
                              <input type="hidden" value="{{$beneficiary_id}}" name="beneficiary_id">
                              <input id="fname" type="hidden" name="fname" value="{{$beneficiary_fname}}">
                              <input id="mname" type="hidden" name="mname" value="{{$beneficiary_mname}}">
                              <input id="lname" type="hidden" name="lname" value="{{$beneficiary_lname}}">
                              <input id="address" type="hidden" name="address" value="{{$beneficiary_address}}">
                              <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-link"> Link</i></button></td>
                            </form>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>

          



        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#voter_search').DataTable();
} );
</script>

@endsection