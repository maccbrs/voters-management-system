@extends('layouts.app')

@section('pageTitle', 'Voters')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if(!empty($msg))
              <div class="alert alert-success alert-dismissible fade show fading-alert"> {{ $msg }}</div>
            @endif

            <div class="card" style="margin-bottom: 15px;">
              <div class="card-body">
                      <form method="POST" action="{{ route('search') }}" class="form-horizontal p-10">
                          {{ csrf_field() }}
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                      </div>
                      <div class="container">
                        

                      <div class="row">
                        <div class="col-md-2">
                          <p>Search Options:</p>
                        </div>
                        <div class="col-md-2">
                          <div class="btn-group btn-group-sm btn-group-toggle float-left" data-toggle="buttons">
                            <label class="btn btn-info btn-sm active">
                              <input type="radio" name="searchoption" id="option1" autocomplete="off" value="name" checked> Name
                            </label>
                            <label class="btn btn-info btn-sm">
                              <input type="radio" name="searchoption" id="option2" autocomplete="off" value="address"> Address
                            </label>
                          </div>
                        </div>
                      </div>

                      </div>
                      </form>
              </div>
            </div>

            <center>
              <div id="result">
              </div>
            </center>

            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> Voters List</div>
                <div class="card-body">
                  <div class="container">

                    <table class="table table-sm" id="beneficiary_search">
                      <thead>
                        <tr>
                          <th>Comelec ID No.</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Birthdate</th>
                          <th>Gender</th>
                          <th>Voter</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Voter_list))
                        @foreach($Voter_list as $b)
                        <tr>
                          <td>{{$b->comelec_id_no}}</td>
                          <td><a href="{{ route('voter_info', ['id'=>$b->id]) }}">{{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}</a></td>
                          <td>{{$b->address}}</td>
                          <td>{{$b->birthdate}}</td>
                          <td>@if($b->gender == 'M') Male @else Female @endif</td>
                          <td>
                            <div class="btn-group btn-group-toggle voter" data-toggle="buttons">
                              <label class="btn btn-secondary btn-sm @if($b->voter_tag == '1') active @endif">
                                <input type="radio" name="options" id="option1" autocomplete="off" value="1" voter_id="{{$b->id}}" @if($b->voter_tag == '1') checked @endif> Yes
                              </label>
                              <label class="btn btn-secondary btn-sm @if($b->voter_tag == '2') active @endif">
                                <input type="radio" name="options" id="option2" autocomplete="off" value="2" voter_id="{{$b->id}}" @if($b->voter_tag == '2') checked @endif> No
                              </label>
                              <label class="btn btn-secondary btn-sm @if($b->voter_tag == '3') active @endif">
                                <input type="radio" name="options" id="option3" autocomplete="off" value="3" voter_id="{{$b->id}}" @if($b->voter_tag == '3') checked @endif> Maybe
                              </label>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>

                    {{ $Voter_list->appends(request()->input())->links() }}
                  </div>
                </div>
              </div>
            </div>
          </div>




        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#beneficiary_search').DataTable({
    "ordering": false,
    "lengthMenu": [ 10, 20, 30, 40, 50 ],
    "pageLength": 50
  });
} );

$(function(){

    $('.voter').change(function(){
      var selectedVal = $('input[name="options"]:checked').attr("value");
      var voter_id = $('input[name="options"]:checked').attr("voter_id");
        //alert(selectedVal);   

      $.ajax( {
        type: "POST",
        url: "{{url('/voter_tag')}}",
        data: {'_token': '{{ csrf_token() }}', 'options':selectedVal, 'id':voter_id},
    success:function(data){
              $("#result").html('Successfully updated record!'); 
              $("#result").addClass("alert alert-success alert-dismissible fade show fading-alert2");

              $(".fading-alert2").fadeTo(2000, 500).slideUp(500, function(){
              $(".fading-alert2").slideUp(500);
          });

          $('.count').each(function () {
              $(this).prop('Counter',0).animate({
                  Counter: $(this).text()
              }, {
                  duration: 2000,
                  easing: 'swing',
                  step: function (now) {
                      $(this).text(Math.ceil(now));
                  }
              });
          });
    },error:function(){ 
        alert("error!!!!");
    }
      });

    });          

});
</script>

@endsection