@extends('layouts.app')

@section('pageTitle', 'Voters')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if(!empty($msg))
              <div class="alert alert-success alert-dismissible fade show fading-alert"> {{ $msg }}</div>
            @endif

            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> Voters Info</div>
                <div class="card-body">
                  <div class="container">

                    <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Name: </p></div>
                    <div class="col-md-4">
                      <b>{{$Voter_list->name}}</b>
                    </div>
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Comelec ID No.: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->comelec_id_no}}</b></p></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Firstname: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->fname}}</b></p></div>
                    <div class="col-md-2"><p style="margin-bottom: 0px;">ID No.: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->id_no}}</b></p></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Middlename: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->mname}}</b></p></div>
                    <div class="col-md-2"><p style="margin-bottom: 0px;">District: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->district}}</b></p></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Lastname: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->lname}}</b></p></div>
                    <div class="col-md-2">Precinct:</div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->precint}}</b></p></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Birthdate: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->birthdate}}</b></p>
                    </div>
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Gender: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->gender}}</b></p></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Address: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Voter_list->address}}</b></p></div>
                    <div class="col-md-2"><p style="margin-bottom: 0px;">Status: </p></div>
                    <div class="col-md-4"><p style="margin-bottom: 0px;"><b>@if($Voter_list->status == '1') Active @else Inactive @endif</b></p></div>
                  </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
          <div class="container">
            <h6>Services Benifited</h6>
            <hr/>
          </div>
          </div> 

          @if(!empty($Voter_list->burial_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> BURIAL BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->burial_beneficiaries->fetch_availments))
                      <tr>
                        <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->burial_beneficiaries->id, 'service'=>'burial']) }}">{{strtoupper($Voter_list->burial_beneficiaries->lname.', '.$Voter_list->burial_beneficiaries->fname.' '.$Voter_list->burial_beneficiaries->mname)}}</a></td>
                        <td>@if (!empty($Voter_list->burial_beneficiaries->voters_info)){{$Voter_list->burial_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->burial_beneficiaries->address)?$Voter_list->burial_beneficiaries->address: 'No Data'}} @endif</td>
                        <td>{{$Voter_list->burial_beneficiaries->fetch_availments->sum('amount')}}</td>
                        <td>@if($Voter_list->burial_beneficiaries->status == '1') Active @else Inactive @endif</td>
                        <td>@if(!empty($Voter_list->burial_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                        <td>@if($Voter_list->burial_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->ched_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> CHED BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->ched_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->ched_beneficiaries->id, 'service'=>'ched']) }}">{{strtoupper($Voter_list->ched_beneficiaries->lname.', '.$Voter_list->ched_beneficiaries->fname.' '.$Voter_list->ched_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->ched_beneficiaries->voters_info)){{$Voter_list->ched_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->ched_beneficiaries->address)?$Voter_list->ched_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->ched_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->ched_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->ched_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->ched_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->dfa_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> DFA BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->dfa_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->dfa_beneficiaries->id, 'service'=>'dfa']) }}">{{strtoupper($Voter_list->dfa_beneficiaries->lname.', '.$Voter_list->dfa_beneficiaries->fname.' '.$Voter_list->dfa_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->dfa_beneficiaries->voters_info)){{$Voter_list->dfa_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->dfa_beneficiaries->address)?$Voter_list->dfa_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->dfa_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->dfa_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->dfa_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->dfa_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->financial_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> FINANCIAL BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->financial_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->financial_beneficiaries->id, 'service'=>'financial']) }}">{{strtoupper($Voter_list->financial_beneficiaries->lname.', '.$Voter_list->financial_beneficiaries->fname.' '.$Voter_list->financial_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->financial_beneficiaries->voters_info)){{$Voter_list->financial_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->financial_beneficiaries->address)?$Voter_list->financial_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->financial_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->financial_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->financial_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->gip_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> GIP BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->gip_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->gip_beneficiaries->id, 'service'=>'livelihood']) }}">@if(!empty($Voter_list->gip_beneficiaries->lname) && !empty($Voter_list->gip_beneficiaries->fname)) {{strtoupper($Voter_list->gip_beneficiaries->lname.', '.$Voter_list->gip_beneficiaries->fname.' '.$Voter_list->gip_beneficiaries->mname)}}@else{{strtoupper($Voter_list->gip_beneficiaries->name)}}@endif</a></td>
                          <td>@if (!empty($Voter_list->gip_beneficiaries->voters_info)){{$Voter_list->gip_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->gip_beneficiaries->address)?$Voter_list->gip_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->gip_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->gip_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->gip_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->gip_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->legal_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> LEGAL BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->legal_beneficiaries->fetch_availments))
                      @foreach($Voter_list->legal_beneficiaries as $b)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->legal_beneficiaries->id, 'service'=>'legal']) }}">{{strtoupper($Voter_list->legal_beneficiaries->lname.', '.$Voter_list->legal_beneficiaries->fname.' '.$Voter_list->legal_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->legal_beneficiaries->voters_info)){{$Voter_list->legal_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->legal_beneficiaries->address)?$Voter_list->legal_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->legal_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->legal_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->legal_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->legal_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->medical_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> MEDICAL BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->medical_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->medical_beneficiaries->id, 'service'=>'medical']) }}">{{strtoupper($Voter_list->medical_beneficiaries->lname.', '.$Voter_list->medical_beneficiaries->fname.' '.$Voter_list->medical_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->medical_beneficiaries->voters_info)){{$Voter_list->medical_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->medical_beneficiaries->address)?$Voter_list->medical_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->medical_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->medical_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->medical_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->medical_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->pcso_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> PCSO BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->pcso_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->pcso_beneficiaries->id, 'service'=>'pcso']) }}">{{strtoupper($Voter_list->pcso_beneficiaries->lname.', '.$Voter_list->pcso_beneficiaries->fname.' '.$Voter_list->pcso_beneficiaries->mname)}}</a></td>
                          <td>@if (!empty($Voter_list->pcso_beneficiaries->voters_info)){{$Voter_list->pcso_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->pcso_beneficiaries->address)?$Voter_list->pcso_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->pcso_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->pcso_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->pcso_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->pcso_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        @if(!empty($Voter_list->qfs_beneficiaries))
          <div class="row">
          <div class="container">
            <div class="card bg-light mb-3">
              <div class="card-header"><i class="fas fa-columns"></i> QFS BENFICIARY</div>
              <div class="card-body">
                <div class="container">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Recipient</th>
                        <th>Address</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Voter Link</th>
                        <th>Voter Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(count($Voter_list->qfs_beneficiaries->fetch_availments))
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$Voter_list->qfs_beneficiaries->id, 'service'=>'qfs']) }}">@if(!empty($Voter_list->qfs_beneficiaries->lname) && !empty($Voter_list->qfs_beneficiaries->fname)) {{strtoupper($Voter_list->qfs_beneficiaries->lname.', '.$Voter_list->qfs_beneficiaries->fname.' '.$Voter_list->qfs_beneficiaries->mname)}}@else{{strtoupper($Voter_list->qfs_beneficiaries->name)}}@endif</a></td>
                          <td>@if (!empty($Voter_list->qfs_beneficiaries->voters_info)){{$Voter_list->qfs_beneficiaries->voters_info->address}} @else {{!empty($Voter_list->qfs_beneficiaries->address)?$Voter_list->qfs_beneficiaries->address: 'No Data'}} @endif</td>
                          <td>{{$Voter_list->qfs_beneficiaries->fetch_availments->sum('amount')}}</td>
                          <td>@if($Voter_list->qfs_beneficiaries->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($Voter_list->qfs_beneficiaries->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($Voter_list->qfs_beneficiaries->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif

        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
$(document).ready(function() {
  $('.table').DataTable({
    "ordering": false,
    "lengthMenu": [ 10, 20, 30, 40, 50 ],
    "pageLength": 50
  });
});
</script>
@endsection