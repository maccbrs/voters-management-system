@extends('layouts.app')

@section('pageTitle', 'Search Beneficiary')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if(!empty($msg))
              <div class="alert alert-success alert-dismissible fade show fading-alert"> {{ $msg }}</div>
            @endif

            <div class="card" style="margin-bottom: 15px;">
              <div class="card-body">
                      <form method="get" action="{{ url('/search_all')}}" class="form-horizontal p-10">
                          {{ csrf_field() }}
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                      </div>
                      <div class="container">
                        

                      <div class="row">
                        <div class="col-md-2">
                          <p>Search Options:</p>
                        </div>
                        <div class="col-md-2">
                          <div class="btn-group btn-group-sm btn-group-toggle float-left" data-toggle="buttons">
                            <label class="btn btn-info btn-sm active">
                              <input type="radio" name="searchoption" id="option1" autocomplete="off" value="name" checked> Name
                            </label>
                            <label class="btn btn-info btn-sm">
                              <input type="radio" name="searchoption" id="option2" autocomplete="off" value="address"> Address
                            </label>
                          </div>
                        </div>
                      </div>

                      </div>
                      </form>
              </div>
            </div>


        </div>
    </div>
</div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> Beneficiary List</div>
                <div class="card-body">
                  <div class="container">
<!--                     <div class="btn-group">
                      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_beneficiary"><i class="fas fa-plus"> Add Beneficiary</i></button>
                    </div>
                    <hr/> -->
                    <table class="table table-sm" id="beneficiary_search">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Service</th>
                          <th>Address</th>
                          <th>Total Amount</th>
                          <th>Status</th>
                          <th>Voters Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Beneficiary))
                        @foreach($Beneficiary as $b)
                        <tr>
                          @if($b->getTable() == 'gip' or $b->getTable() == 'qfs')
                          <td><a href="{{ route('beneficiary_view', ['id'=>$b->id, 'service'=>$b->getTable()]) }}">{{$b->name}}</a></td>
                          @else
                          <td><a href="{{ route('beneficiary_view', ['id'=>$b->id, 'service'=>$b->getTable()]) }}">{{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}</a></td>
                          @endif
                          <td>{{strtoupper($b->getTable())}}</td>
                          <td>{{!empty($b->address)?$b->address: 'No Data'}}</td>
                          <td>@if(is_numeric($b->fetch_availments)) {{!empty($b->fetch_availments)?$b->fetch_availments->sum('amount'): ' '}}
                          @else
                          @endif
                          </td>
                          <td>@if($b->status == '1') Active @else Inactive @endif</td>
                          <td>@if($b->voters_status == 0) Non-Voter @elseif($b->voters_status == 1) Voter @elseif($b->voters_status == 2) New @endif</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#beneficiary_search').DataTable({
    "ordering": false,
    "lengthMenu": [ 10, 20, 30, 40, 50 ],
    "pageLength": 50
  });
} );
</script>

@endsection