@extends('layouts.app')

@section('header-style')

@section('pageTitle', 'View')

@section('subPage', 'Beneficiary')

@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}
</style>

    <!-- The Modal -->
                        <div class="modal fade" id="edit_availment">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title">Edit Availment History</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!-- Modal body -->
                              <div class="modal-body">
                                
                                <div class="row">

                                <div class="col-md-12">

                                <form action="{{ URL('/update_availment') }}" method="POST">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="service" value="{{$service}}">
                                <input type="hidden" name="beneficiary_id" id="hidden_edit" value="">
                                <div class="form-group custom_bottom">
                                  <label for="request_date" class="custom_bottom">Request Date</label>
                                  <input class="form-control form-control-sm" id="request_date" name="request_date" type="date" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="service_type" class="custom_bottom">Service Type</label>
                                  <input class="form-control form-control-sm" id="service_type" name="service_type" type="text" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="amount" class="custom_bottom">Amount</label>
                                  <input class="form-control form-control-sm" id="amount" name="amount" type="number" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="remarks" class="custom_bottom">Remarks</label>
                                  <input class="form-control form-control-sm" id="remarks" name="remarks" type="text" value="">
                                </div>

                              </div>

                              </div>

                            </div>
                              
                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>

                          <!-- Modal -->
                          <div class="modal fade" id="delete_avail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  Are you sure you want to delete this history?
                                </div>
                                <div class="modal-footer">
                                  <form action="{{ URL('/delete_availment') }}" method="POST">
                                  {{ csrf_field() }}
                                  <input type="hidden" id="hidden_delete" value="" name="availment_id">
                                  <input type="hidden" name="service" value="{{$service}}">
                                  <button type="submit" class="btn btn-primary">Yes</button>
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>

                            <!-- The Modal -->
                        <div class="modal fade" id="add_availment">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header">
                                <h4 class="modal-title">Add Availment</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!-- Modal body -->
                              <div class="modal-body">
                                
                                <div class="row">

                                <div class="col-md-12">

                                <form action="{{ URL('/add_availment') }}" method="POST" autocomplete="off">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="service" value="{{$service}}">
                                  <input type="hidden" name="beneficiary_id" value="{{!empty($Beneficiary->id)?$Beneficiary->id: ' '}}">

                                  <input class="form-control form-control-sm" id="fname" name="fname" type="hidden" value="{{!empty($Beneficiary->fname)?$Beneficiary->fname: ' '}}">

                                  <input class="form-control form-control-sm" id="mname" name="mname" type="hidden" value="{{!empty($Beneficiary->mname)?$Beneficiary->mname: ' '}}">

                                  <input class="form-control form-control-sm" id="lname" name="lname" type="hidden" value="{{!empty($Beneficiary->lname)?$Beneficiary->lname: ' '}}">

                                  <input class="form-control form-control-sm" id="dob" name="dob" type="hidden" value="{{!empty($Beneficiary->dob)?$Beneficiary->dob: ' '}}">

                                  <input class="form-control form-control-sm" id="address" name="address" type="hidden" value="{{!empty($Beneficiary->address)?$Beneficiary->address: ' '}}">

                                  <input class="form-control form-control-sm" id="barangay" name="barangay" type="hidden" value="{{!empty($Beneficiary->barangay)?$Beneficiary->barangay: ' '}}">

                                  @if($service != 'ched')
                                  <input class="form-control form-control-sm" id="district" name="district" type="hidden" value="{{!empty($Beneficiary->district)?$Beneficiary->district: ' '}}">
                                  @endif

                                  <input class="form-control form-control-sm" id="sex" name="sex" type="hidden" value="{{!empty($Beneficiary->sex)?$Beneficiary->sex: ' '}}">

                                  <input class="form-control form-control-sm" id="mobile_number" name="mobile_number" type="hidden" value="{{!empty($Beneficiary->mobile_number)?$Beneficiary->mobile_number: ' '}}">

                                  <input class="form-control form-control-sm" id="email" name="email" type="hidden" value="{{!empty($Beneficiary->email)?$Beneficiary->email: ' '}}">

                                <div class="form-group custom_bottom">
                                  <label for="request_date" class="custom_bottom">Request Date</label>
                                  <input class="form-control form-control-sm" id="request_date" name="request_date" type="date" value="">
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="service_type" class="custom_bottom">Service Type</label>
                                  <input class="form-control form-control-sm" id="service_type" name="service_type" type="text" value="">
                                </div>

                                @if($service != 'ched')
                                <div class="form-group custom_bottom">
                                  <label for="particulars" class="custom_bottom">Particulars</label>
                                  <input class="form-control form-control-sm" id="particulars" name="particulars" type="text" value="">
                                </div>
                                @endif

                                <div class="form-group custom_bottom">
                                  <label for="Institution" class="custom_bottom">Institution</label>
                                  <input class="form-control form-control-sm" id="Institution" name="Institution" type="text" value="">
                                </div>

                                @if($service == 'ched')
                                <div class="form-group custom_bottom">
                                  <label for="Institution" class="custom_bottom">Year</label>
                                  <input class="form-control form-control-sm" id="Institution" name="school_yr" type="text" value="">
                                </div>
                                @endif

                                <div class="form-group custom_bottom">
                                  <label for="amount" class="custom_bottom">Amount</label>
                                  <input class="form-control form-control-sm" id="amount" name="amount" type="number" value="">
                                </div>

                                  <input class="form-control form-control-sm" id="action_officer" name="action_officer" type="hidden" value="{{ Auth::user()->id }}">

                                <div class="form-group custom_bottom">
                                  <label for="recommendation" class="custom_bottom">Recommendations</label>
                                  <textarea class="form-control form-control-sm" id="recommendation" name="recommendation"></textarea>
                                </div>

                                <div class="form-group custom_bottom">
                                  <label for="remarks" class="custom_bottom">Remarks</label>
                                  <input class="form-control form-control-sm" id="remarks" name="remarks" type="text" value="">
                                </div>


                              </div>

                              </div>

                            </div>
                              
                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                              
                            </div>
                          </div>
                        </div>

                      </form>

<div class="modal fade" id="add_voter">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Voter's Info</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

            <div class="card" style="margin-bottom: 15px;">
              <div class="card-body">
                      <form method="get" action="{{ url('/search_voter')}}" class="form-horizontal p-10" autocomplete="off">
                          {{ csrf_field() }}
                      <input id="fname" type="hidden" name="fname" value="{{!empty($Beneficiary->fname)?$Beneficiary->fname: ' '}}">
                      <input id="mname" type="hidden" name="mname" value="{{!empty($Beneficiary->mname)?$Beneficiary->mname: ' '}}">
                      <input id="lname" type="hidden" name="lname" value="{{!empty($Beneficiary->lname)?$Beneficiary->lname: ' '}}">
                      <input id="address" type="hidden" name="address" value="{{!empty($Beneficiary->address)?$Beneficiary->address: ' '}}">
                      <input type="hidden" name="beneficiary_id" value="{{!empty($Beneficiary->id)?$Beneficiary->id: ' '}}">
                      <input type="hidden" name="service" value="{{$service}}">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                      </div>
                      <div class="container">
                        

                      <div class="row">
                        <div class="col-md-2">
                          <p>Search Options:</p>
                        </div>
                        <div class="col-md-2">
                          <div class="btn-group btn-group-sm btn-group-toggle float-left" data-toggle="buttons">
                            <label class="btn btn-info btn-sm active">
                              <input type="radio" name="searchoption" id="option1" autocomplete="off" value="name" checked> Name
                            </label>
                            <label class="btn btn-info btn-sm">
                              <input type="radio" name="searchoption" id="option2" autocomplete="off" value="comelecid"> Comelec ID
                            </label>
                          </div>
                        </div>
                      </div>

                      </div>
                      </form>
              </div>
            </div>

        </div>
        
      </div>
    </div>
  </div>


  <!-- The Modal -->
  <div class="modal fade" id="edit_info">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Info</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
          <div class="row">

          <div class="col-md-6">

          <h5>Beneficiary Info</h5>
          <hr/>

          <form action="{{ URL('/update_info') }}" method="POST">
            {{ csrf_field() }}

            <input type="hidden" name="service" value="{{$service}}">

          <input type="hidden" name="beneficiary_id" value="{{!empty($Beneficiary->id)?$Beneficiary->id: ''}}">
          <div class="form-group custom_bottom">
            <label for="fname" class="custom_bottom">First Name</label>
            <input class="form-control form-control-sm" id="fname" name="fname" type="text" value="{{!empty($Beneficiary->fname)?$Beneficiary->fname: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="mname" class="custom_bottom">Middle Name</label>
            <input class="form-control form-control-sm" id="mname" name="mname" type="text" value="{{!empty($Beneficiary->mname)?$Beneficiary->mname: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="lname" class="custom_bottom">Last Name</label>
            <input class="form-control form-control-sm" id="lname" name="lname" type="text" value="{{!empty($Beneficiary->lname)?$Beneficiary->lname: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="bod" class="custom_bottom">Birthdate</label>
            <input class="form-control form-control-sm" id="bod" name="dob" type="date" value="{{!empty($Beneficiary->dob)?$Beneficiary->dob: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="address" class="custom_bottom">Address</label>
            <input class="form-control form-control-sm" id="address" name="address" type="text" value="{{!empty($Beneficiary->address)?$Beneficiary->address: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="barangay" class="custom_bottom">Barangay</label>
            <select class="form-control" id="barangay" name="barangay_id">
              <option disabled selected>* Please Select Barangay</option>
              @foreach($Barangays as $barangay)
              <option value="{{$barangay->id}}">{{$barangay->barangay}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group custom_bottom">
            <label for="mobile_number" class="custom_bottom">Mobile Number</label>
            <input class="form-control form-control-sm" id="mobile_number" name="mobile_number" type="number" value="{{!empty($Beneficiary->mobile_number)?$Beneficiary->mobile_number: ''}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="mobile_number" class="custom_bottom">Email</label>
            <input class="form-control form-control-sm" id="email" type="email" name="email" value="{{!empty($Beneficiary->email)?$Beneficiary->email: ''}}">
          </div>

        </div>

          <div class="col-md-6">
          <h5>Voter's Info</h5>
          <hr/>

        @if(!empty($Beneficiary->voters_info))

          <input type="hidden" name="voter_id" value="{{!empty($Beneficiary->voters_info->id)?$Beneficiary->voters_info->id: ''}}">
          <div class="form-group custom_bottom">
            <label for="idno" class="custom_bottom">ID No</label>
            <input class="form-control form-control-sm" id="idno" name="idno" type="text" value="{{!empty($Beneficiary->voters_info->id_no)?$Beneficiary->voters_info->id_no: 'No Data'}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="comelecid" class="custom_bottom">Comelec ID No</label>
            <input class="form-control form-control-sm" id="comelecid" name="comelecid" type="text" value="{{!empty($Beneficiary->voters_info->comelec_id_no)?$Beneficiary->voters_info->comelec_id_no: 'No Data'}}">
          </div>

          <div class="form-group custom_bottom">
            <label for="presict" class="custom_bottom">Precint</label>
            <input class="form-control form-control-sm" id="precint" name="presict" type="text" value="{{!empty($Beneficiary->voters_info->presict)?$Beneficiary->voters_info->presict: ''}}">
          </div>

        @endif
        </div>

      </div>

      </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        
      </div>
    </div>
  </div>

</form>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ Auth::user()->name }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if (\Session::has('msg'))
            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Success!</strong> Information has been updated.
            </div>
            @endif

              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i>
                  @if(empty($Beneficiary->fname) or empty($Beneficiary->mname) or empty($Beneficiary->lname))
                  {{!empty($Beneficiary->name)?$Beneficiary->name: 'No Data'}}
                  @else
                  {{!empty($Beneficiary->fname)?$Beneficiary->fname: 'No Data'}}, {{!empty($Beneficiary->mname)?$Beneficiary->mname: 'No Data'}} {{!empty($Beneficiary->lname)?$Beneficiary->lname: 'No Data'}}
                        @endif</div>
                <div class="card-body">

                    
                    <div class="row">
                      <hr/>
                    <div class="container">
                    <div class="btn-group">
                      @if(empty($Beneficiary->voters_info))
                      <button type="button" class="btn btn-successs btn-sm" data-toggle="modal" data-target="#add_voter"><i class="fas fa-plus"> Add Voter's Info</i></button>
                      @endif
                      @if(!empty($Beneficiary->voters_info))
                      <form action="{{ URL('/unlink_voter') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="beneficiary_id" value="{{$Beneficiary->id}}">
                        <input type="hidden" name="service" value="{{$service}}">
                      <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-minus"> Unlink Voter's Info</i></button>
                      </form>
                      @endif
                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit_info"><i class="fas fa-edit"> Edit Info</i></button>
                      <form action="{{ URL('/delete_beneficiary') }}" method="POST">
                        {{ csrf_field() }}
                      <input type="hidden" name="beneficiary_id" value="{{$Beneficiary->id}}">
                      <input type="hidden" name="service" value="{{$service}}">
                      <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-user-minus"> Delete Beneficiary</i></button>
                      </form>
                    </div>
                    <hr/>
                    </div>   
                  </div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Name: </p></div>
  <div class="col-md-4">
                        @if($service == 'livelihood' or $service == 'qfs' or $service == 'gip')
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->name)?$Beneficiary->name: ''}}</b></p>
                        @else
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->fname)?$Beneficiary->fname: ''}} {{!empty($Beneficiary->mname)?$Beneficiary->mname: ''}} {{!empty($Beneficiary->lname)?$Beneficiary->lname: ''}}</b></p>
                        @endif
  </div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Code: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->id)?$Beneficiary->voters_info->id: 'No Data'}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Firstname: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->fname)?$Beneficiary->fname: 'No Data'}}</b></p></div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">ID No.: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->id_no)?$Beneficiary->voters_info->id_no: 'No Data'}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Middlename: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->mname)?$Beneficiary->mname: 'No Data'}}</b></p></div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Comelec ID No.: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->comelec_id_no)?$Beneficiary->voters_info->comelec_id_no: 'No Data'}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Lastname: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->lname)?$Beneficiary->lname: 'No Data'}}</b></p></div>
  <div class="col-md-2">Mobile No.:</div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->mobile_number)?$Beneficiary->mobile_number: 'No Data'}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Birthdate: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>
                          @if($Beneficiary->voters_id != 0 or $Beneficiary->voters_id != NULL)
                            {{!empty($Beneficiary->voters_info->birthdate)?$Beneficiary->voters_info->birthdate: 'No Data'}}
                          @else
                            @if($Beneficiary->dob == '1970-01-01')
                              No Data
                            @else
                            {{$Beneficiary->dob}}
                            @endif
                          @endif
                        </b>
                      </p>
  </div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Email: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Beneficiary->email}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Address: </p></div>
  <div class="col-md-4">
                        <p style="margin-bottom: 0px;"><b>
                          @if($Beneficiary->voters_id != 0 or $Beneficiary->voters_id != NULL)
                            {{!empty($Beneficiary->voters_info->address)?$Beneficiary->voters_info->address: 'No Data'}}
                          @else
                            {{!empty($Beneficiary->address)?$Beneficiary->address: 'No Data'}}
                          @endif
                        </b></p>
  </div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Status: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>@if($Beneficiary->status == '1') Active @else Inactive @endif</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Barangay: </p></div>
  <div class="col-md-4">
                          <p style="margin-bottom: 0px;"><b>@if(!empty($Beneficiary->barangay_id))
                                                          {{$Beneficiary->fetch_barangay->barangay}} 
                                                          @else
                                                          {{!empty($Beneficiary->barangay)?$Beneficiary->barangay: 'No Data'}}
                                                          @endif</b></p>
  </div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Remarks: </p></div>
  <div class="col-md-4"><p style="margin-bottom: 0px;"><b>{{$Beneficiary->remarks}}</b></p></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">District: </p></div>
  <div class="col-md-4">
 <p style="margin-bottom: 0px;"><b>@if(!empty($Beneficiary->barangay_id))
                                                          {{$Beneficiary->fetch_barangay->district}} 
                                                          @else
                                                          {{!empty($Beneficiary->district)?$Beneficiary->district: 'No Data'}}
                                                          @endif</b></p>
  </div>
  <div class="col-md-2"><p style="margin-bottom: 0px;">Voter Status: </p></div>
  <div class="col-md-4">
     <p style="margin-bottom: 0px;"><b>@if($Beneficiary->voters_status == 0) Non-Voter @elseif($Beneficiary->voters_status == 1) Voter @elseif($Beneficiary->voters_status == 2) New @endif</b></p>
  </div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Sex: </p></div>
  <div class="col-md-4">
    <p style="margin-bottom: 0px;"><b>@if(!empty($Beneficiary->voters_info)) {{$Beneficiary->voters_info->gender}} @else {{!empty($Beneficiary->sex)?$Beneficiary->sex: ''}} @endif</b></p>
  </div>
  <div class="col-md-2"></div>
  <div class="col-md-4"></div>
</div>

<div class="row">
  <div class="col-md-2"><p style="margin-bottom: 0px;">Precint: </p></div>
  <div class="col-md-4">
    <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->precint)?$Beneficiary->voters_info->precint: 'No Data'}}</b></p>
  </div>
  <div class="col-md-2"></div>
  <div class="col-md-4"></div>
</div>
<!--                   <div class="container">
                    <div class="row">
                      <div class="col-md-3">
                        @if($service == 'livelihood' or $service == 'qfs' or $service == 'gip')
                        <p style="margin-bottom: 0px;">Name: </p>
                        @else
                        <p style="margin-bottom: 0px;">Firstname: </p>
                        <p style="margin-bottom: 0px;">Middlename: </p>
                        <p style="margin-bottom: 0px;">LastName: </p>
                        @endif
                        <p style="margin-bottom: 0px;">Birthdate: </p>
                        <p style="margin-bottom: 0px;">Address: </p>
                        <p style="margin-bottom: 0px;">Barangay: </p>
                        <p style="margin-bottom: 0px;">District: </p>
                        <p style="margin-bottom: 0px;">Sex: </p>
                        <p style="margin-bottom: 0px;">Precint: </p>
                      </div>
                      <div class="col-md-3">
                        @if($service == 'livelihood' or $service == 'qfs' or $service == 'gip')
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->name)?$Beneficiary->name: 'No Data'}}</b></p>
                        @else
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->fname)?$Beneficiary->fname: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->mname)?$Beneficiary->mname: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->lname)?$Beneficiary->lname: 'No Data'}}</b></p>
                        @endif
                        <p style="margin-bottom: 0px;"><b>
                          @if($Beneficiary->voters_id != 0 or $Beneficiary->voters_id != NULL)
                            {{!empty($Beneficiary->voters_info->birthdate)?$Beneficiary->voters_info->birthdate: 'No Data'}}
                          @else
                            @if($Beneficiary->dob == '1970-01-01')
                              No Data
                            @else
                            {{$Beneficiary->dob}}
                            @endif
                          @endif
                        </b></p>
                        <p style="margin-bottom: 0px;"><b>
                          @if($Beneficiary->voters_id != 0 or $Beneficiary->voters_id != NULL)
                            {{!empty($Beneficiary->voters_info->address)?$Beneficiary->voters_info->address: 'No Data'}}
                          @else
                            {{!empty($Beneficiary->dob)?$Beneficiary->dob: 'No Data'}}
                          @endif
                        </b></p>
                        <p style="margin-bottom: 0px;"><b>@if(!empty($Beneficiary->barangay_id))
                                                          {{$Beneficiary->fetch_barangay->barangay}} 
                                                          @else
                                                          {{!empty($Beneficiary->barangay)?$Beneficiary->barangay: 'No Data'}}
                                                          @endif</b></p>
                        <p style="margin-bottom: 0px;"><b>@if(!empty($Beneficiary->barangay_id))
                                                          {{$Beneficiary->fetch_barangay->district}} 
                                                          @else
                                                          {{!empty($Beneficiary->district)?$Beneficiary->district: 'No Data'}}
                                                          @endif</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->sex)?$Beneficiary->sex: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->precint)?$Beneficiary->voters_info->precint: 'No Data'}}</b></p>
                      </div>
                      <div class="col-md-3">
                        <p style="margin-bottom: 0px;">Code: </p>
                        <p style="margin-bottom: 0px;">ID No.: </p>
                        <p style="margin-bottom: 0px;">Comelec ID No.: </p>
                        <p style="margin-bottom: 0px;">Mobile No.:</p>
                        <p style="margin-bottom: 0px;">Email:</p>
                        <p style="margin-bottom: 0px;">Status:</p>
                        <p style="margin-bottom: 0px;">Remarks:</p>
                      </div>
                      <div class="col-md-3">
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->id)?$Beneficiary->voters_info->id: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->id_no)?$Beneficiary->voters_info->id_no: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->voters_info->comelec_id_no)?$Beneficiary->voters_info->comelec_id_no: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{!empty($Beneficiary->mobile_number)?$Beneficiary->mobile_number: 'No Data'}}</b></p>
                        <p style="margin-bottom: 0px;"><b>{{$Beneficiary->email}}</b></p>
                        <p style="margin-bottom: 0px;"><b>@if($Beneficiary->status == '1') Active @else Inactive @endif</b></p>
                        <p style="margin-bottom: 0px;"><b>{{$Beneficiary->remarks}}</b></p>
                      </div>
                    </div>
                  </div> -->
                </div>
              </div>


            @if (\Session::has('avail_msg'))
            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Success!</strong> Information has been updated.
            </div>
            @endif

            @if (\Session::has('del_msg'))
            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Success!</strong> Availment has been added.
            </div>
            @endif

            @if (\Session::has('add_msg'))
            <div class="alert alert-success alert-dismissible fade show fading-alert">
              <strong>Success!</strong> Availment has been deleted.
            </div>
            @endif
              <div class="card bg-light mb-3">

<!--                 <div class="card-header"><i class="fas fa-columns">AVAILMENT HISTORY</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div> -->
                @if($service == 'ched')
                <div class="card-header"><i class="fas fa-columns"> CHED</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'qfs')
                <div class="card-header"><i class="fas fa-columns"> QFS</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'medical')
                <div class="card-header"><i class="fas fa-columns"> MEDICAL</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'financial')
                <div class="card-header"><i class="fas fa-columns"> FINANCIAL</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'burial')
                <div class="card-header"><i class="fas fa-columns"> BURIAL</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'dfa')
                <div class="card-header"><i class="fas fa-columns"> DFA</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'pcso')
                <div class="card-header"><i class="fas fa-columns"> PCSO-PAGIBIG</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'legal')
                <div class="card-header"><i class="fas fa-columns"> LEGAL AND FINANCIAL</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'livelihood')
                <div class="card-header"><i class="fas fa-columns"> LIVELIHOOD</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @elseif($service == 'college')
                <div class="card-header"><i class="fas fa-columns"> COLLEGE</i> 
                  <a data-toggle="collapse" class="float-right" href="#social-block" aria-expanded="true" aria-controls="test-block">
                      <i class="fas fa-arrows-alt-v"></i>
                  </a>
                </div>
                @endif


                <div id="social-block" @if(empty($Availments)) class="collapse" @endif >
                <div class="card-body">
                  <h5 class="card-title">Services Availments</h4>
                    <hr/>
                    <div class="btn-group">
                      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#add_availment"><i class="fas fa-plus"> Add Availment</i></button>
                    </div>
                    <hr/>  
                    <table class="table table-sm">
                    <thead>
                      <tr>
                        <th>Request date</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Requested by</th>
                        <th>Remarks</th>
                        <th>Actions</th>
                        <th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(!empty($Availments))
                      @foreach($Availments as $b)
                      <tr>
                        @if($service == 'ched' or $service == 'qfs')
                        <td>{{$b->date}}</td>
                        @else
                        <td>{{$b->request_date}}</td>
                        @endif
                        <td>{{!empty($b->service_type)?$b->service_type: strtoupper(substr($b->getTable(), 0, strpos($b->getTable(), "_")))}}</td>
                        <td>{{!empty($b->amount)?$b->amount: '0'}}</td>
                        <td>{{!empty($b->fetch_officer->name)?$b->fetch_officer->name: 'N/A'}}
                        <td>{{!empty($b->remarks)?$b->remarks: 'No Data'}}</td>
                        <td>
                        

                      </form>
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="$('#hidden_edit').val('{{$b->id}}');$('#request_date').val('{{!empty($b->request_date)?$b->request_date: $b->date}}');$('#service_type').val('{{!empty($b->service_type)?$b->service_type: strtoupper(substr($b->getTable(), 0, strpos($b->getTable(), '_')))}}');$('#amount').val('{{$b->amount}}');" data-target="#edit_availment">Edit</button>
                          <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_avail" onclick="$('#hidden_delete').val('{{$b->id}}');$('#hidden_service'.val('{{$service}}'));" data-target="#edit_availment">Delete</button>
                        </div>
                        </td>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>                      
                    </table>
                </div>
              </div>
              </div>

        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
</script>

@endsection