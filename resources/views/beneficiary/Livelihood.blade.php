@extends('layouts.app')

@section('pageTitle', 'Livelihood')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')

<style type="text/css">
.custom_bottom
{
  margin-bottom: 2px;
}

@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>

                            <!-- The Modal -->
                        <div class="modal fade" id="add_beneficiary">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                            
                              <!-- Modal Header -->
                              <div class="modal-header text-white bg-info mb-3">
                                <h4 class="modal-title">Add Beneficiary</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              
                              <!-- Modal body -->
                              <div class="modal-body">
                                
                                <div class="row">

                                <div class="col-md-12">
                                <form action="{{ URL('/add_beneficiary') }}" method="POST" autocomplete="off">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="service" value="livelihood">
                                  
                                  <div class="row">

                                  <div class="col-md-12">
                                    <h6 class="card-subtitle mb-2 text-muted">Benificiary Information</h6>
                                    <hr/>
                                  </div>
                                    <div class="col-md-6">
                                      <div class="form-group custom_bottom">
                                      <label for="fname" class="custom_bottom">First Name</label>
                                      <input class="form-control form-control-sm" id="fname" name="fname" type="text" value="" placeholder="First Name">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="mname" class="custom_bottom">Middle Name</label>
                                      <input class="form-control form-control-sm" id="mname" name="mname" type="text" value="" placeholder="Middle Name">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="lname" class="custom_bottom">Last Name</label>
                                      <input class="form-control form-control-sm" id="lname" name="lname" type="text" value="" placeholder="Last Name">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="dob" class="custom_bottom">Date of Birth</label>
                                      <input class="form-control form-control-sm" id="dob" name="dob" type="date" value="">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="sex" class="custom_bottom">Sex</label>
                                      <input class="form-control form-control-sm" id="sex" name="sex" type="text" value="" placeholder="Sex">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group custom_bottom">
                                      <label for="mobile_number" class="custom_bottom">Mobile Number</label>
                                      <input class="form-control form-control-sm" id="mobile_number" name="mobile_number" type="text" value="" placeholder="Mobile Number">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="email" class="custom_bottom">Email</label>
                                      <input class="form-control form-control-sm" id="email" name="email" type="email" value="" placeholder="Email">
                                      </div>

                                      <div class="form-group custom_bottom">
                                      <label for="address" class="custom_bottom">Address</label>
                                      <input class="form-control form-control-sm" id="address" name="address" type="text" value="" placeholder="Address">
                                      </div>

                                      <div class="form-group custom_bottom">
                                        <label for="barangay" class="custom_bottom">Barangay</label>
                                        <select class="form-control form-control-sm" id="barangay" name="barangay_id">
                                          <option disabled selected>* Please Select Barangay</option>
                                          @foreach($Barangays as $barangay)
                                          <option value="{{$barangay->id}}">{{$barangay->barangay}}</option>
                                          @endforeach
                                        </select>
                                      </div>

                                    <div class="form-group custom_bottom">
                                      <label for="request_date" class="custom_bottom">Request Date</label>
                                      <input class="form-control form-control-sm" id="request_date" name="request_date" type="date" value="">
                                    </div>
                                    </div>
                                  </div>



                                <div class="row">
                                  <div class="col-md-12">
                                    <h6 class="card-subtitle mb-2 text-muted" style="margin-top: 15px;">Benificiary Service</h6>
                                    <hr/>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group custom_bottom">
                                      <label for="service_type" class="custom_bottom">Service Type</label>
                                      <input class="form-control form-control-sm" id="service_type" name="service_type" type="text" value="Livelihood" readonly="" placeholder="Service Type">
                                    </div>

                                    <div class="form-group custom_bottom">
                                      <label for="particulars" class="custom_bottom">Particulars</label>
                                      <input class="form-control form-control-sm" id="particulars" name="particulars" type="text" value="" placeholder="Particulars">
                                    </div>

                                    <div class="form-group custom_bottom">
                                      <label for="Institution" class="custom_bottom">Institution</label>
                                      <input class="form-control form-control-sm" id="Institution" name="Institution" type="text" value="" placeholder="Institution">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group custom_bottom">
                                      <label for="amount" class="custom_bottom">Amount</label>
                                      <input class="form-control form-control-sm" id="amount" name="amount" type="number" value="" placeholder="Amount">
                                    </div>

                                      <input class="form-control form-control-sm" id="action_officer" name="action_officer" type="hidden" value="{{ !empty(Auth::user()->id)?Auth::user()->id: '' }}">

                                    <div class="form-group custom_bottom">
                                      <label for="recommendation" class="custom_bottom">Recommendations</label>
                                      <textarea class="form-control form-control-sm" id="recommendation" name="recommendation" placeholder="Recommendations"></textarea>
                                    </div>

                                    <div class="form-group custom_bottom">
                                      <label for="remarks" class="custom_bottom">Remarks</label>
                                      <input class="form-control form-control-sm" id="remarks" name="remarks" type="text" value="" placeholder="Remarks">
                                    </div>
                                  </div>
                                </div>


                              </div>


                              </div>

                            </div>
                              
                              <!-- Modal footer -->
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                              
                            </div>
                          </div>
                        </div>

                      </form>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-success alert-dismissible fade show fading-alert" role="alert">
              <strong>Welcome!</strong> You are logged in as {{ !empty(Auth::user()->name)?Auth::user()->name: '' }}.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            @if(!empty($msg))
              <div class="alert alert-success alert-dismissible fade show fading-alert"> {{ $msg }}</div>
            @endif

            <div class="card" style="margin-bottom: 15px;">
              <div class="card-body">
                      <form method="get" action="{{ url('/search_beneficiary')}}" class="form-horizontal p-10">
                          {{ csrf_field() }}
                          <input type="hidden" name="service" value="{{$service}}">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                      </div>
                      <div class="container">
                        

                      <div class="row">
                        <div class="col-md-2">
                          <p>Search Options:</p>
                        </div>
                        <div class="col-md-2">
                          <div class="btn-group btn-group-sm btn-group-toggle float-left" data-toggle="buttons">
                            <label class="btn btn-info btn-sm active">
                              <input type="radio" name="searchoption" id="option1" autocomplete="off" value="name" checked> Name
                            </label>
                            <label class="btn btn-info btn-sm">
                              <input type="radio" name="searchoption" id="option2" autocomplete="off" value="address"> Address
                            </label>
                          </div>
                        </div>
                      </div>

                      </div>
                      </form>
              </div>
            </div>


            <div class="row">
            <div class="container">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-columns"></i> LIVELIHOOD LIST <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#add_beneficiary"><i class="fas fa-plus"> Add Beneficiary</i></button></div>
                <div class="card-body">
                  <div class="container">
                    <table class="table table-sm" id="beneficiary_search">
                      <thead>
                        <tr>
                          <th>Recipient</th>
                          <th>Address</th>
                          <th>Total Amount</th>
                          <th>Status</th>
                          <th>Voter Link</th>
                          <th>Voter Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($Ched_list))
                        @foreach($Ched_list as $b)
                        <tr>
                          <td><a href="{{ route('beneficiary_view', ['id'=>$b->id, 'service'=>'livelihood']) }}">@if(!empty($b->lname) && !empty($b->fname)) {{strtoupper($b->lname.', '.$b->fname.' '.$b->mname)}}@else{{strtoupper($b->name)}}@endif</a></td>
                          <td>@if (!empty($b->voters_info)){{$b->voters_info->address}} @else {{!empty($b->address)?$b->address: 'No Data'}} @endif</td>
                          <td>{{$b->fetch_availments->sum('amount')}}</td>
                          <td>@if($b->status == '1') Active @else Inactive @endif</td>
                          <td>@if(!empty($b->voters_id)) Linked @else Not Linked @endif</td>
                          <td>@if($b->voters_status == 1) Up-to-date @else Non-Voter @endif</td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>

                    {{ $Ched_list->appends(request()->input())->links() }}
                  </div>
                </div>
              </div>
            </div>
          </div>




        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
<script>
    $(".fading-alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".fading-alert").slideUp(500);
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$(document).ready(function() {
  $('#beneficiary_search').DataTable({
    "ordering": false,
    "lengthMenu": [ 10, 20, 30, 40, 50 ],
    "pageLength": 50
  });
} );
</script>

@endsection