<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Q-CRM</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('bootstrap-4.1.3/css/bootstrap.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('datatables/datatables.min.css') }}">

    <style type="text/css">
            .page-footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <!-- <div class="container"> -->
                <a class="navbar-brand" href="{{ url('/home') }}">
                <img src="{{ asset('logo/test.jpg') }}" alt="Q-CRM" style="width: 30px; height: 30px;">
                    Q-CRM
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @if(Auth::User())
<!--                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Beneficiaries
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">All Beneficiaries</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Registered Voters</a>
                                  <a class="dropdown-item" href="#">Non-Registered Voters</a>
                            </div>
                        </li> -->

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/home') }}"><i class="fa fa-home fa-fw"></i> Home<span class="sr-only">(current)</span></a>
                          </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_beneficiary" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Beneficiary
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown_beneficiary">
                                <a class="dropdown-item" href="{{ url('/search_all') }}">ALL</a>
                                <div class="dropdown-divider"></div>
                                <div class="dropdown-header">Services</div>
                                <a class="dropdown-item" href="{{ url('/ched') }}">CHED</a>
                                <a class="dropdown-item" href="{{ url('/qfs') }}">QFS</a>
                                <a class="dropdown-item" href="{{ url('/medical') }}">MEDICAL</a>
                                <a class="dropdown-item" href="{{ url('/fa') }}">FINANCIAL</a>
                                <a class="dropdown-item" href="{{ url('/ba') }}">BURIAL</a>
                                <a class="dropdown-item" href="{{ url('/dfa') }}">DFA</a>
                                <a class="dropdown-item" href="{{ url('/pcso') }}">PCSO-PAGIBIG</a>
                                <a class="dropdown-item" href="{{ url('/legal') }}">LEGAL AND FINANCIAL</a>
                                <a class="dropdown-item" href="{{ url('/livelihood') }}">LIVELIHOOD</a>
                                <a class="dropdown-item" href="{{ url('/college') }}">COLLEGE</a>
<!--                                 @if(Auth::user() && Auth::user()->access == 1) -->
                                @endif
                            </div>

<!--                             <a class="nav-link" href="{{ url('/services') }}">
                                  Beneficiaries
                            </a> -->
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Availments
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown_beneficiary">
<!--                                 <a class="dropdown-item" href="{{ url('/search_reverse_all') }}">ALL</a> -->
<!--                                 <div class="dropdown-divider"></div> -->
                                <div class="dropdown-header">Services</div>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'ched']) }}">CHED</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'qfs']) }}">QFS</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'medical']) }}">MEDICAL</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'financial']) }}">FINANCIAL</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'burial']) }}">BURIAL</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'dfa']) }}">DFA</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'pcso']) }}">PCSO-PAGIBIG</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'legal']) }}">LEGAL AND FINANCIAL</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'livelihood']) }}">LIVELIHOOD</a>
                                <a class="dropdown-item" href="{{ route('reverse', ['service'=>'college']) }}">COLLEGE</a>
<!--                                 @if(Auth::user() && Auth::user()->access == 1) -->
                                @endif
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Tools
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/report') }}">Reports</a>
                                @if(Auth::user() && Auth::user()->access == 1)
                                <div class="dropdown-divider"></div>
                                <small><div class="dropdown-header">Admin Tools</div></small>
                                <a class="dropdown-item" href="{{ url('/user_management') }}">User Management</a>
                                <a class="dropdown-item" href="{{ url('/announcement') }}">Announcement</a>
                                @endif
                            </div>
                        </li>
                        @endif


                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <?php /*<li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li> */ ?>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            <!-- </div> -->
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

    </div>

        <!-- Footer -->
        <footer class="blue">
            <!-- Copyright -->
            <div class="text-center py-3"><small>© 2015-2018. Office of Rep. Romero Federico Quimbo, House of Representatives, Republic of the Philippines. All Rights Reserved. | 
            <a href="#"> QCRM.com</a></small>
            </div>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->

    @yield('footer-scripts')
</body>
</html>
