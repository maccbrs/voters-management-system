@extends('layouts.app')

@section('pageTitle', 'Charts')

@section('header-style')
<style type="text/css">

</style>
@endsection

@section('content')
<div class="container">

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Ched Graph</div>
                  <div class="card-body">
                  <div id="ched"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Qfs Graph</div>
                  <div class="card-body">
                  <div id="qfs"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Medical Graph</div>
                  <div class="card-body">
                  <div id="medical"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Financial Graph</div>
                  <div class="card-body">
                  <div id="financial"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Burial Graph</div>
                  <div class="card-body">
                  <div id="burial"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> DFA Graph</div>
                  <div class="card-body">
                  <div id="dfa"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> PCSO Graph</div>
                  <div class="card-body">
                  <div id="pcso"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Legal Graph</div>
                  <div class="card-body">
                  <div id="legal"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> Livelihood Graph</div>
                  <div class="card-body">
                  <div id="livelihood"></div>
                  </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card bg-light mb-3">
                <div class="card-header"><i class="fas fa-folder-open"></i> College Graph</div>
                  <div class="card-body">
                  <div id="college"></div>
                  </div>
              </div>
            </div>
          </div>

      </div>

@endsection

@section('footer-scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  
  function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

</script>

      <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});     
      </script>

      <script>
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Ched_barangay as $cb)
               ['{{$cb->barangay}}', {{$cb->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'CHED Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('ched'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script>
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Qfs_barangay as $qfs)
               ['{{$qfs->barangay}}', {{$qfs->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'QFS Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('qfs'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Medical_barangay as $mb)
               ['{{$mb->barangay}}', {{$mb->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'MEDICAL Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('medical'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Financial_barangay as $fb)
               ['{{$fb->barangay}}', {{$fb->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'FINANCIAL Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('financial'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Burial_barangay as $bb)
               ['{{$bb->barangay}}', {{$bb->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'BURIAL Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('burial'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Dfa_barangay as $db)
               ['{{$db->barangay}}', {{$db->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'DFA Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('dfa'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Pcso_barangay as $pb)
               ['{{$pb->barangay}}', {{$pb->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'PCSO Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('pcso'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @if(!empty($Legal_barangay))
               @foreach($Legal_barangay as $lb)
               ['{{$lb->barangay}}', {{$lb->total}}, getRandomColor()],
               @endforeach
               @endif
            ]);

            var options = {title: 'LEGAL Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('legal'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($Gip_barangay as $gp)
               ['{{$gp->address}}', {{$gp->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'LIVELIHOOD Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('livelihood'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

      <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Barangay', 'Barangay', { role: 'style' }],
               @foreach($College_barangay as $col_b)
               ['{{$col_b->barangay}}', {{$col_b->total}}, getRandomColor()],
               @endforeach
            ]);

            var options = {title: 'COLLEGE Beneficiaries'}; 

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('college'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>

@endsection