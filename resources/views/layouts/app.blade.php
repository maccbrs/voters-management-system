<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Q-CRM</title>

    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="{{ asset('bootstrap-4.1.3/css/bootstrap.css') }}">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('datatables/Buttons-1.5.2/css/buttons.bootstrap.min.css') }}" rel="stylesheet">

  </head>
  @if(Auth::User())
  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" @if(Request::url() == url('/home')) href="#" @else href="{{url('/home')}}" @endif>QCRM</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
<!--       <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form> -->

      <!-- Navbar -->
      <ul class="navbar-nav d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
<!--         <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger">9+</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-envelope fa-fw"></i>
            <span class="badge badge-danger">7</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li> -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::User()->name }}
            <i class="fas fa-user-circle fa-fw fa-lg"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
<!--             <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="#">Activity Log</a>
            <div class="dropdown-divider"></div> -->
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal"><i class="fas fa-sign-out-alt"></i> Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="{{ url('/home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Beneficiary</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="{{ url('/search_all') }}"><small>ALL BENEFICIARIES</small></a>
            <div class="dropdown-divider"></div>
            <small><div class="dropdown-header">Services</div></small>
            <a class="dropdown-item" href="{{ url('/ched') }}"><small>CHED</small></a>
            <a class="dropdown-item" href="{{ url('/qfs') }}"><small>QFS</small></a>
            <a class="dropdown-item" href="{{ url('/medical') }}"><small>MEDICAL</small></a>
            <a class="dropdown-item" href="{{ url('/fa') }}"><small>FINANCIAL</small></a>
            <a class="dropdown-item" href="{{ url('/ba') }}"><small>BURIAL</small></a>
            <a class="dropdown-item" href="{{ url('/dfa') }}"><small>DFA</small></a>
            <a class="dropdown-item" href="{{ url('/pcso') }}"><small>PCSO-PAGIBIG</small></a>
            <a class="dropdown-item" href="{{ url('/legal') }}"  style="white-space: normal;"><small>LEGAL AND FINANCIAL</small></a>
            <a class="dropdown-item" href="{{ url('/livelihood') }}"><small>LIVELIHOOD</small></a>
            <a class="dropdown-item" href="{{ url('/college') }}"><small>COLLEGE</small></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Availments</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <small><div class="dropdown-header">Services</div></small>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'ched']) }}"><small>CHED</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'qfs']) }}"><small>QFS</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'medical']) }}"><small>MEDICAL</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'financial']) }}"><small>FINANCIAL</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'burial']) }}"><small>BURIAL</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'dfa']) }}"><small>DFA</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'pcso']) }}"><small>PCSO-PAGIBIG</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'legal']) }}" style="white-space: normal;"><small>LEGAL AND FINANCIAL</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'livelihood']) }}"><small>LIVELIHOOD</small></a>
            <a class="dropdown-item" href="{{ route('reverse', ['service'=>'college']) }}"><small>COLLEGE</small></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-file-export"></i>
            <span>Reports</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
<!--             <small><div class="dropdown-header">Total Voters</div></small>
            <a class="dropdown-item" href="{{ url('/voter_precinct') }}" style="white-space: normal;"><small>List of all Voters per Precinct</small></a> -->
            <small><div class="dropdown-header">Beneficiaries</div></small>
            <a class="dropdown-item" href="{{ route('beneficiary_street', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Beneficiaries per Street</small></a>
            <a class="dropdown-item" href="{{ route('beneficiary_barangay', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Beneficiaries per Barangay</small></a>
            <a class="dropdown-item" href="{{ route('beneficiary_precinct', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Beneficiaries per Precinct</small></a>
            <div class="dropdown-divider"></div>
            <small><div class="dropdown-header">Q Families</div></small>
            <a class="dropdown-item" href="{{ route('qfamily_street', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Q Families per Street</small></a>
            <a class="dropdown-item" href="{{ route('qfamily_barangay', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Q Families per Barangay</small></a>
            <a class="dropdown-item" href="{{ route('qfamily_precinct', ['service'=>'all']) }}" style="white-space: normal;"><small>List of Q Families per Precinct</small></a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-cogs"></i>
            <span>Tools</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            @if(Auth::user() && Auth::user()->access == 1)
            <small><div class="dropdown-header">Admin Tools</div></small>
            <a class="dropdown-item" href="{{ url('/user_management') }}"><small>User Management</small></a>
            <a class="dropdown-item" href="{{ url('/announcement') }}"><small>Announcement</small></a>
            <div class="dropdown-divider"></div>
            <small><div class="dropdown-header">Linking Tools</div></small>
            <a class="dropdown-item" href="{{ route('link', ['status'=>'new']) }}"><small>Link New</small></a>
            <a class="dropdown-item" href="{{ route('link', ['status'=>'voter']) }}"><small>Link Voter</small></a>
            <a class="dropdown-item" href="{{ route('link', ['status'=>'nonvoter']) }}"><small>Link Non-Voter</small></a>
            @endif
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/voters') }}">
            <i class="fas fa-fw fa-user-circle"></i>
            <span>Voters</span></a>
        </li>        
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/charts') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
        </li>
<!--         <li class="nav-item">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
        </li> -->
      </ul>
    @endif

      <div id="content-wrapper">
        @if(Auth::User())
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ url('/home') }}"><i class="fas fa-columns"></i> Dashboard</a>
            </li>
            @if(View::hasSection('subPage'))
            <li class="breadcrumb-item">@yield('subPage')</li>
            @endif
            <li class="breadcrumb-item active">@yield('pageTitle')</li>
          </ol>
        </div>
        @endif

        @yield('content')

        <!-- Sticky Footer -->
        @if(Auth::User())
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>© 2015-2018. Office of Rep. Romero Federico Quimbo, House of Representatives, Republic of the Philippines. All Rights Reserved.</span>
            </div>
          </div>
        </footer>
        @endif

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
          </div>
        </div>
      </div>
    </div>



  </body>

      <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Page level plugin JavaScript-->
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('datatables/Buttons-1.5.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ asset('datatables/Buttons-1.5.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('datatables/Buttons-1.5.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('datatables/Buttons-1.5.2/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>



    <!-- Demo scripts for this page-->
<!--     <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script> -->

@yield('footer-scripts')



</html>
