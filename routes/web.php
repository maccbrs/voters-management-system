<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/charts', 'HomeController@charts')->name('charts');

Auth::routes();

Route::group(['middleware' => 'auth'], function (){

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user_management', 'admin\UserManagementController@index')->name('usermanagement');
Route::post('/add_user', 'admin\UserManagementController@add_user')->name('add_user');

Route::get('/beneficiary_precinct/{service}', 'report\ReportController@beneficiary_precinct')->name('beneficiary_precinct');
Route::get('/beneficiary_barangay/{service}', 'report\ReportController@beneficiary_barangay')->name('beneficiary_barangay');
Route::get('/voter_precinct', 'report\ReportController@voter_precinct')->name('voter_precinct');
Route::get('/beneficiary_street/{service}', 'report\ReportController@beneficiary_street')->name('beneficiary_street');
/*Route::get('/voter_precinct', 'report\ReportController@export')->name('export');*/
Route::get('/q_family', 'report\ReportController@q_family')->name('q_family');
Route::get('/qfamily_street/{service}', 'report\ReportController@qfamily_street')->name('qfamily_street');
Route::get('/qfamily_barangay/{service}', 'report\ReportController@qfamily_barangay')->name('qfamily_barangay');
Route::get('/qfamily_precinct/{service}', 'report\ReportController@qfamily_precinct')->name('qfamily_precinct');
Route::post('/add_query', 'report\ReportController@add_query')->name('add_query');
Route::get('/generated_report', 'report\ReportController@generate_report')->name('generated_report');


Route::post('/reset', 'admin\UserManagementController@resetpassword')->name('resetpassword');

Route::post('/change_access', 'admin\UserManagementController@change_access')->name('change_access');

Route::get('/services', 'beneficiary\SocialServicesController@index')->name('services');


Route::get('/ched', 'beneficiary\ChedController@index')->name('ched');
Route::get('/view/id/{id}', 'beneficiary\ChedController@beneficiary')->name('ched_beneficiary_view');

Route::get('/ba', 'beneficiary\BaController@index')->name('ba');
Route::get('/view/id/{id}', 'beneficiary\BaController@beneficiary')->name('ba_beneficiary_view');

Route::get('/college', 'beneficiary\CollegeController@index')->name('college');
Route::get('/view/id/{id}', 'beneficiary\CollegeController@beneficiary')->name('college_beneficiary_view');

Route::get('/dfa', 'beneficiary\DfaController@index')->name('dfa');
Route::get('/view/id/{id}', 'beneficiary\DfaController@beneficiary')->name('Fa_beneficiary_view');

Route::get('/fa', 'beneficiary\FaController@index')->name('financial');
Route::get('/view/id/{id}', 'beneficiary\FaController@beneficiary')->name('Fa_beneficiary_view');

Route::get('/legal', 'beneficiary\LegalController@index')->name('legal');
Route::get('/view/id/{id}', 'beneficiary\LegalController@beneficiary')->name('legal_beneficiary_view');

Route::get('/livelihood', 'beneficiary\LivelihoodController@index')->name('livelihood');
Route::get('/view/id/{id}', 'beneficiary\LivelihoodController@beneficiary')->name('livelihood_beneficiary_view');

Route::get('/medical', 'beneficiary\MedicalController@index')->name('medical');
Route::get('/view/id/{id}', 'beneficiary\MedicalController@beneficiary')->name('medical_beneficiary_view');

Route::get('/pcso', 'beneficiary\PcsoController@index')->name('pcso');
Route::get('/view/id/{id}', 'beneficiary\PcsoController@beneficiary')->name('pcso_beneficiary_view');

Route::get('/qfs', 'beneficiary\QfsController@index')->name('qfs');
Route::get('/view/id/{id}', 'beneficiary\QfsController@beneficiary')->name('qfs_beneficiary_view');


Route::get('/view/{service}/id/{id}', 'beneficiary\SocialServicesController@beneficiary')->name('beneficiary_view');


Route::get('/search_beneficiary', 'beneficiary\SocialServicesController@search')->name('search_beneficiary');

Route::get('/search_voter', 'beneficiary\SocialServicesController@search_voter')->name('search_voter');
Route::get('/voters', 'voter\VoterController@index')->name('voters');
Route::post('/voter_search', 'voter\VoterController@search')->name('search');
Route::post('/voter_tag', 'voter\VoterController@tag')->name('tag');
Route::get('/voters/export', 'voter\VoterController@export');
Route::get('/voter/view/{id}', 'voter\VoterController@voter_info')->name('voter_info');

Route::post('/link_voter', 'beneficiary\SocialServicesController@link_voter')->name('link_voter');
Route::post('/non_voter', 'beneficiary\SocialServicesController@non_voter')->name('non_voter');
Route::post('/unlink_voter', 'beneficiary\SocialServicesController@unlink_voter')->name('unlink_voter');

Route::post('/update_info', 'beneficiary\SocialServicesController@update_info')->name('update_info');

Route::post('/delete_beneficiary', 'beneficiary\SocialServicesController@delete_beneficiary')->name('delete_beneficiary');

Route::post('/update_availment', 'beneficiary\SocialServicesController@update_availment')->name('update_availment');

Route::post('/delete_availment', 'beneficiary\SocialServicesController@delete_availment')->name('delete_availment');

Route::post('/add_availment', 'beneficiary\SocialServicesController@add_availment')->name('add_availment');

Route::post('/add_beneficiary', 'beneficiary\SocialServicesController@add_beneficiary')->name('add_beneficiary');

Route::get('/announcement', 'admin\UserManagementController@announcement')->name('announcement');
Route::post('/post_announce', 'admin\UserManagementController@post_announce')->name('post_announce');

Route::get('/utility', 'admin\UserManagementController@utility')->name('utility');
Route::get('/utility_2', 'admin\UserManagementController@utility_2')->name('utility_2');
Route::get('/utility_3', 'admin\UserManagementController@utility_3')->name('utility_3');
Route::get('/utility_4', 'admin\UserManagementController@utility_4')->name('utility_4');
Route::get('/utility_5', 'admin\UserManagementController@utility_5')->name('utility_5');
Route::get('/utility_6', 'admin\UserManagementController@utility_6')->name('utility_6');
Route::get('/utility_7', 'admin\UserManagementController@utility_7')->name('utility_7');
Route::get('/utility_8', 'admin\UserManagementController@utility_8')->name('utility_8');

Route::get('/reverse/{service}', 'beneficiary\ReverseController@index')->name('reverse');
Route::post('/reverse_link', 'beneficiary\ReverseController@reverse_link')->name('reverse_link');
Route::post('/link_beneficiary', 'beneficiary\ReverseController@link_beneficiary')->name('link_beneficiary');
Route::post('/add_beneficiary_reverse', 'beneficiary\ReverseController@add_beneficiary_reverse')->name('add_beneficiary_reverse');
Route::post('/search_availments', 'beneficiary\ReverseController@search_availments')->name('search_availments');

Route::get('/master', 'beneficiary\SocialServicesController@master')->name('master');
Route::get('/search_all', 'beneficiary\SocialServicesController@search_all')->name('search_all');
Route::get('/search_reverse_all', 'beneficiary\SocialServicesController@search_reverse_all')->name('search_reverse_all');
Route::post('/search_reverse_view', 'beneficiary\ReverseController@search_reverse_view')->name('search_reverse_view');

Route::get('/link/{status}', 'link\LinkController@index')->name('link');
Route::get('/link_voter', 'link\LinkController@link_voter')->name('link_voter');

});